#include "stdafx.h"
#include <booster/types.h>
#include <booster/IndexedDirectory.h>
#include <booster/FileSystem.h>
#include <booster/extensions.h>
#include <booster/std_filesystem.h>
#include <booster/utils.h>

using namespace Booster;

std::unordered_map<std::string, SP<IndexedDirectory>> IndexedDirectory::_cache;
std::mutex IndexedDirectory::_cache_lock;

IndexedDirectory::IndexedDirectory(const std::string& key, const std::string& directory, const std::string& extension) :
	_key(key),
	_directory(directory),
	_extension(extension),
	_sequence(-1),
	seq_filename("__seq__" + extension)
{
}

i64 IndexedDirectory::initSequence()
{
	if (_sequence < 0)
	{
		std::lock_guard<std::mutex> _(_lock);
		initSequenceNoLock();
	}
	return _sequence;
}

void IndexedDirectory::dispose()
{
	std::lock_guard<std::mutex> _(_lock);
	_sequence = -1;

	if (_seq_stream.is_open())
		_seq_stream.close();
}



i64 IndexedDirectory::initSequenceNoLock()
{
	if(_sequence < 0)
	{
		FileSystem::ensurePath(_directory);
		std::string filename = _directory + seq_filename;
		if(FileSystem::openOrCreate(filename, std::fstream::binary | std::fstream::in | std::fstream::out, _seq_stream))
		{
			_seq_stream.seekg(0, std::ios::beg);
			_seq_stream.read(reinterpret_cast<char*>(&_sequence), sizeof(i64));
		}

		if (_sequence < 0)
		{
			i64 seq = 0;

			for (auto &p : fs::directory_iterator(_directory))
			{
				fs::path path(p);

				if($(path.filename().string()).equalsNoCase(seq_filename) || path.extension() != _extension)
					continue;

				path.replace_extension("");

				try
				{
					i64 s = base36Decode(path.filename().string());
					if (s > seq)
						seq = s;
				}
				catch(...)
				{
				}				
			}
			++seq;
			_seq_stream.seekp(0, std::ios::beg);
			_seq_stream.write(reinterpret_cast<char*>(&seq), sizeof(i64));
			_sequence = seq;
		}
	}
	return _sequence;
}

std::string IndexedDirectory::getNewFilename(bool check_exist)
{
	std::lock_guard<std::mutex> _(_lock);

	initSequenceNoLock();
	std::string res;

	do
	{
		res = _directory + base36Encode(_sequence) + _extension;
		++_sequence;
	}while(res == seq_filename || (check_exist && FileSystem::exists(res)));

	_seq_stream.seekp(0, std::ios::beg);
	_seq_stream.write(reinterpret_cast<char*>(&_sequence), sizeof(i64));

	return res;
}

SP<IndexedDirectory> IndexedDirectory::get(const std::string& path)
{
	fs::path p(path);
	std::string ext = p.extension().string();
	if(ext.empty() || ext == ".")
		ext = "";
	else
		p.replace_extension();

	return get(p.string(), ext);
}

SP<IndexedDirectory> IndexedDirectory::get(const std::string& directory, const std::string& extension)
{
	auto dir = FileSystem::ensureDirectoryEnding(directory);
	auto key = $() << dir << "|" << extension;

	std::lock_guard<std::mutex> _(_cache_lock);
	return $(_cache).getOrAdd(key, [&]
	{
		return std::make_shared<IndexedDirectory>(key, dir, extension);
	});
}

bool IndexedDirectory::remove(SP<IndexedDirectory> dir)
{
	std::lock_guard<std::mutex> _(_cache_lock);
	return _cache.erase(dir->_key) != 0;
}
