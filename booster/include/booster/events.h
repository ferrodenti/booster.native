#pragma once

//https://github.com/cristik/cpp-events
// Copyright (c) 2014-2016, Cristian Kocza
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
// OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
// OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <vector>
#include <functional>

namespace Booster
{
	template<class TSender, typename... TArguments>
	class Event
	{
	public:
		class EventHandler
		{
		public:
			EventHandler(const int tag) : _tag(tag)
			{
			}
			virtual ~EventHandler() = default;
			bool isEqual(EventHandler *other) const
			{
				if (!other)
					return false;
				if (other->_tag != _tag)
					return false;

				return this->checkEqual(other);
			}
			virtual void notify(TSender *sender, TArguments... args) const = 0;
		protected:
			virtual bool checkEqual(EventHandler *other) const = 0;
		private:
			int _tag;
		};

	private:

		class EventHandlerList
		{
		public:
			void add(EventHandler *handler) 
			{
				_handlers.push_back(handler);
			}
			void remove(EventHandler *handler) 
			{
				auto it = _handlers.begin();
				auto end = _handlers.end();

				for(; it != end; ++it) 
					if((*it)->isEqual(handler)) 
						break;

				if(it != end)
				{
					EventHandler *h = *it;
					_handlers.erase(it);
					delete h;
				}
			}
			void notify(TSender *sender, TArguments... args) const
			{
				for(auto handler : _handlers) 
					handler->notify(sender, args...);
			}

			~EventHandlerList()
			{
				for(auto handler : _handlers)
					delete handler;
			}
		private:
			std::vector<EventHandler*> _handlers;
		};
    
		template<class O, class F>
		class InstanceEventHandler : public EventHandler 
		{
		public:
			InstanceEventHandler(O obj, F proc) : 
				EventHandler(1), 
				_obj(obj), 
				_proc(proc)
			{
			}

			void notify(TSender *sender, TArguments... args) const override
			{
				(_obj->*_proc)(sender, args...);
			}
		protected:
			bool checkEqual(EventHandler *other) const override
			{
				auto handler = dynamic_cast<InstanceEventHandler*>(other);
				return handler && _obj == handler->_obj && _proc == handler->_proc;
			}
		private:
			O _obj;
			F _proc;
		};
    
		class StaticEventHandler : public EventHandler
		{
		public:
			typedef void (*F)(TSender*, TArguments...);

			StaticEventHandler(F proc) :
				EventHandler(2), 
				_proc(proc)
			{
			}

			void notify(TSender *sender, TArguments... args) const override
			{
				_proc(sender, args...);
			}
		protected:
			bool checkEqual(EventHandler *other) const override
			{
				auto handler = dynamic_cast<StaticEventHandler*>(other);
				return handler && _proc == handler->_proc;
			}
		private:
			F _proc;
		};
    
		class FunctionEventHandler : public EventHandler
		{
		public:
			typedef std::function<void (TSender*, TArguments...)> F;

			FunctionEventHandler(F proc) : 
				EventHandler(3), 
				_proc(proc)
			{
			}

			void notify(TSender *sender, TArguments... args) const override
			{
				_proc(sender, args...);
			}
		protected:
			bool checkEqual(EventHandler *other) const override
			{
				return false;
			}
		private:
			F _proc;
		};
    
		TSender *_sender;
		EventHandlerList _handler_list;
		friend TSender;

	protected:
		Event(TSender *sender) :
			_sender(sender)
		{
		}

		void operator()(TArguments... args) 
		{
			_handler_list.notify(_sender, args...);
		}

		void add(EventHandler* handler)
		{
			_handler_list.add(handler);
		}
		void remove(EventHandler* handler)
		{
			_handler_list.remove(handler);
		}

	public:
		template<class O, class F>
		void operator +=(std::pair<O, F> p) 
		{
			add(new InstanceEventHandler<O, F>(p.first, p.second));
		}
    
		template<class O, class F>
		void operator -=(std::pair<O, F> p) 
		{
			auto handler = InstanceEventHandler<O, F>(p.first, p.second);
			remove(&handler);
		}
    
		void operator +=(void (*f)(TSender*, TArguments...)) 
		{
			add(new StaticEventHandler(f));
		}
    
		void operator -=(void (*f)(TSender*, TArguments...)) 
		{
			auto handler = StaticEventHandler(f);
			remove(&handler);
		}
    
    
		void operator +=(std::function<void (TSender*, TArguments...)> f) 
		{
			add(new FunctionEventHandler(f));
		}
    
		template<class F>
		void operator -=(std::function<void (TSender*, TArguments...)> f) 
		{
		}
	};

	// ReSharper disable once CppInconsistentNaming
	template<class O, class F>
	std::pair<O, F> EventHandler(O o, F f)
	{
		return std::pair<O, F>(o, f);
	}
}