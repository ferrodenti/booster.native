#pragma once

namespace Booster
{
	class BinaryStream 
	{
		template<typename T>
		struct identity { typedef T type; };

		SP<std::istream> _istream;
		SP<std::ostream> _ostream;

		void write(std::string value, identity<std::string>)
		{
			size_t sz = value.size();
			writeSize(sz);
			_ostream->write(value.c_str(), sz);
		}

		template<typename T>
		void write(T value, identity<T>)
		{
			_ostream->write(reinterpret_cast<const char*>(&value), sizeof value);
		}

		std::string read(identity<std::string>);

		template<typename T>
		T read(identity<T>)
		{
			T res;
			_istream->read(reinterpret_cast<char*>(&res), sizeof (T));
			return res;
		}

	public:		
		static const size_t max_size = 0x1FFFFFFFFFFFFFFFul;
		bool shorten_sizes = true;

		std::mutex read_lock;
		std::mutex write_lock;

		template<class T>
		BinaryStream(T& stream) : BinaryStream(makeNotShared<T>(&stream))
		{
		}

		template<class T>
		BinaryStream(SP<T> stream)
		{
			_istream = std::dynamic_pointer_cast<std::istream>(stream);
			_ostream = std::dynamic_pointer_cast<std::ostream>(stream);
		}

		BinaryStream(const std::string& filename, std::ios::openmode open_mode);
		BinaryStream(const BinaryStream& proto);

		//const BinaryStream& operator=(const BinaryStream& other) const;

		template<typename T>
		void write(T value)
		{
			write(value, identity<T>());
		}
		
		template<typename T>
		T read()
		{
			return read(identity<T>());
		}

		void writeSize(size_t size);
		size_t readSize();
		std::string readString(size_t min, size_t max);

		void writeRaw(const std::string& str);
		void writeRaw(const char* buffer, size_t size);
		void readRaw(char* buffer, size_t size);
		std::string readRaw(size_t size);
		std::string readRest();

		void flush();
		bool eof();

		size_t tellRead();
		size_t tellWrite();
		void seekRead(size_t pos);
		void seekWrite(size_t pos);
		void seekRead(size_t pos, std::ios_base::seekdir origin);
		void seekWrite(size_t pos, std::ios_base::seekdir origin);
	};
}
