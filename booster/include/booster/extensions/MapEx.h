#pragma once

#include "BaseEx.h"
#include "DictMethods.h"

namespace Booster
{
	template<typename TKey, typename TValue>
	class MapEx :
		public BaseEx<std::map<TKey, TValue>>,
		public DictMethods<TKey, TValue, MapEx<TKey, TValue>>
	{
		class enumerator : public IEnumerator<std::pair<TKey, TValue >>
		{
			typename std::map<TKey, TValue>::iterator _begin;
			typename std::map<TKey, TValue>::iterator _it;
			typename std::map<TKey, TValue>::iterator _end;
			std::pair<TKey, TValue> _current;
		public:
			enumerator(MapEx<TKey, TValue >& dict) :
				_begin(dict.begin()),
				_end(dict.end())
			{
			}

			const std::pair<TKey, TValue>& current() const override
			{
				return _current;
			}
			bool moveNext() override
			{
				if (_it != _end)
				{
					_current = make_pair(_it->first, _it->second);
					++_it;
					return true;
				}
				return false;
			}
			void reset() override
			{
				_it = _begin;
			}
		};

	public:

		std::shared_ptr<IEnumerator<std::pair<TKey, TValue>>> getEnumerator() const
		{
			return std::make_shared<enumerator>(const_cast<MapEx<TKey, TValue>&>(*this));
		}

		operator std::map<TKey, TValue>() const { return *this; }
		operator Enumerable<std::pair<TKey, TValue>>() const { return Enumerable<std::pair<TKey, TValue>>([=] { return getEnumerator(); }); }

#pragma region IEnumerable<T>

		bool any(std::function<bool(const std::pair<TKey, TValue>&)> condition) const { return any(*this, condition); }
		bool all(std::function<bool(const std::pair<TKey, TValue>&)> condition) const { return all(*this, condition); }

		std::map<TKey, TValue> toMap() const
		{
			return *this;
		}
		std::vector<std::pair<TKey, TValue>> toVector() const
		{
			std::vector<std::pair<TKey, TValue>> res;
			for (auto& p : *this)
				res.push_back(make_pair(p->first, p->second));

			return res;
		}

#pragma endregion 


#pragma region static implementations

		static bool any(const std::map<TKey, TValue>& vector, std::function<bool(const std::pair<TKey, TValue>&)> condition)
		{
			auto& end = vector.end();
			return std::any_of(vector.begin(), end, condition);
		}

		static bool all(const std::map<TKey, TValue>& vector, std::function<bool(const std::pair<TKey, TValue>&)> condition)
		{
			auto& end = vector.end();
			return std::all_of(vector.begin(), end, condition);
		}
#pragma endregion 
	};
}
