#pragma once
#include <booster/linq/LinqMethods.h>

namespace Booster
{
	// ReSharper disable once CppInconsistentNaming
	namespace __dict_methods_details
	{
		template<typename TDict, typename TKey, typename TVal, typename TRealVal>
		typename std::enable_if<! std::is_pointer<TRealVal>::value, bool>::type
		tryGetValue(TDict* dict, const TKey& key, TVal*&  result)
		{
			auto it = dict->find(key);
			if (it != dict->end())
			{
				//result = const_cast<TVal*>(&std::move(it->second));
                result = const_cast<TVal*>(&it->second);
				return true;
			}
			return false;
		}

		template<typename TDict, typename TKey, typename TVal, typename TRealVal>
		typename std::enable_if<std::is_pointer<TRealVal>::value, bool>::type
		tryGetValue(TDict* dict, const TKey& key, TVal*&  result)
		{
			auto it = dict->find(key);
			if (it != dict->end())
			{
				result = const_cast<TVal*>(it->second);
				return true;
			}
			return false;
		}

		template<typename TDict, typename TKey, typename TVal, typename TRealVal>
		typename std::enable_if<!std::is_pointer<TRealVal>::value, bool>::type
			tryGetValueConst(TDict* dict, const TKey& key, const TVal*&  result)
		{
			auto it = dict->find(key);
			if (it != dict->end())
			{
				//result = &std::move(it->second);
                result = &it->second;
				return true;
			}
			return false;
		}

		template<typename TDict, typename TKey, typename TVal, typename TRealVal>
		typename std::enable_if<std::is_pointer<TRealVal>::value, bool>::type
			tryGetValueConst(TDict* dict, const TKey& key, const TVal*&  result)
		{
			auto it = dict->find(key);
			if (it != dict->end())
			{
				result = it->second;
				return true;
			}
			return false;
		}

		template<typename TDict, typename TKey, typename TVal, typename TRealVal>
		typename std::enable_if<!std::is_pointer<TRealVal>::value, TVal&>::type
			getOrAdd(TDict* dict, const TKey& key, std::function<const TVal*()> creator)
		{
			TVal* res;
			if (!tryGetValue<TDict, TKey, TVal, TRealVal>(dict, key, res))
			{
				res = const_cast<TVal*>(creator());
				dict->add(key, *res);
			}
			return *res;
		}

		template<typename TDict, typename TKey, typename TVal, typename TRealVal>
		typename std::enable_if<std::is_pointer<TRealVal>::value, TVal&>::type
			getOrAdd(TDict* dict, const TKey& key, std::function<const TVal*()> creator)
		{
			TVal* res;
			if (!tryGetValue<TDict, TKey, TVal, TRealVal>(dict, key, res))
			{
				res = const_cast<TVal*>(creator());
				dict->add(key, res);
			}
			return *res;
		}
	}

	template<typename TKey, typename TValue, typename TThis>
	class DictMethods : public LinqMethods<std::pair<TKey, TValue>, TThis>
	{
		using ThisPair = std::pair<TKey, TValue>;
		using RawValueType = typename std::remove_const< typename std::remove_reference<typename std::remove_pointer<TValue>::type>::type>::type;

		TThis* dict() const
		{
			// ReSharper disable once CppCStyleCast
			return ((TThis*)this);
		}

	public:
		Enumerable<TKey> keys() const
		{
			return dict()->template select<TKey>([](auto& pair) { return pair.first; });
		}
		Enumerable<TValue> values() const
		{
			return dict()->template select<TValue>([](auto& pair) { return pair.second; });
		}

		bool containsKey(const TKey& key) const
		{
			auto d = dict();
			return d->find(key) != d->end();
		}

		void add(const TKey& key, const TValue& value)
		{
			dict()->insert(std::pair<TKey, TValue>(key, value));
		}

		template<typename TKeyValuePairs>
		i64 addRange(const TKeyValuePairs& pairs)
		{
			i64 i=0;
			for(auto& pair : pairs)
			{
				dict()->insert(pair);
				++i;
			}
			return i;
		}

		bool tryGetValue(const TKey& key, RawValueType*&  result) const
		{
			return __dict_methods_details::tryGetValue<TThis, TKey, RawValueType, TValue>(dict(), key, result);
		}

		bool tryGetValue(const TKey& key, const RawValueType*&  result) const
		{
			return __dict_methods_details::tryGetValueConst<TThis, TKey, RawValueType, TValue>(dict(), key, result);
		}

		RawValueType& getOrAdd(const TKey& key, std::function<const RawValueType*()> creator)
		{
			return __dict_methods_details::getOrAdd<TThis, TKey, RawValueType, TValue>(dict(), key, creator);
		}

		template<class = std::enable_if_t<std::is_copy_assignable<TValue>::value>>
		bool tryGetValue(const TKey& key, TValue&  result) const
		{
			auto d = dict();
			auto it = d->find(key);
			if (it != d->end())
			{
				result = it->second;
				return true;
			}
			return false;
		}
			
		template<class = std::enable_if_t<std::is_copy_assignable<TValue>::value>>
		RawValueType& getOrAdd(const TKey& key, std::function<const TValue()> creator)
		{
			RawValueType* res;
			if (!tryGetValue(key, res))
			{
				auto d = dict();
				std::pair<TKey, TValue> new_val(key, creator());
				d->insert(new_val);
				return d->find(key)->second;
			}
			return *res;
		}


		TValue get(const TKey& key, TValue def = TValue()) const
		{
			TValue res;
			if(tryGetValue(key, res))
				return res;

			return def;
		}
	};
}
