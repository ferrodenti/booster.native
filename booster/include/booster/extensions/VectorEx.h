#pragma once

#include "BaseEx.h"
#include "../linq/Enumerable.h"

namespace Booster
{
	template<typename T>
	class VectorEx : public BaseEx<std::vector<T>>, public LinqMethods<T, VectorEx<T>>
	{
		class enumerator : public IEnumerator<T>
		{
			size_t _pos = -1;
			const VectorEx<T>& _vector;
		public:
			enumerator(const VectorEx<T>& vector) : _vector(vector)
			{
			}

			const T& current() const override
			{
				return _vector[_pos];
			}
			bool moveNext() override
			{
				return ++_pos < _vector.size();
			}
			void reset() override
			{
				_pos = -1;
			}
		};

	public:
		VectorEx() {}
		VectorEx(const std::vector<T>& proto) :
			BaseEx<std::vector<T>>(proto) { }
		VectorEx(const std::initializer_list<T>& init) :
			BaseEx<std::vector<T>>(std::vector<T>(init)) { }
		VectorEx(const Enumerable<T>& enumerable) :
			BaseEx<std::vector<T>>(enumerable.toVector()) { }

		std::shared_ptr<IEnumerator<T>> getEnumerator() const
		{
			return std::make_shared<enumerator>(*this);
		}

		operator std::vector<T>() const { return *this; }
		operator Enumerable<T>() const 
		{
			auto v = *this;
			return Enumerable<T>([v]{ return v.getEnumerator(); }); 
		}

#pragma region IEnumerable<T>

		bool contains(const T& value) const { return contains(*this, value); }
		i64 indexOf(const T& value) const { return indexOf(*this, value); }

		bool any(std::function<bool(const T&)> condition) const { return any(*this, condition); }
		bool all(std::function<bool(const T&)> condition) const { return all(*this, condition); }

		/*bool equals(const std::vector<T>& other) const //TODO:...
		{
			if (this->size() != other.size())
				return false;

			return const_cast<const VectorEx<T>*>(this)->equals($(other));
		}*/

		std::vector<T> toVector() const
		{
			return *this;
		}

#pragma endregion 

		i64 addRange(const std::vector<T>& v)
		{
			i64 cnt = 0;
			for(auto& i : v)
			{
				this->push_back(i);
				cnt ++;
			}
			return cnt;
		}
		i64 addRange(const Enumerable<T>& range) { return range.pushToVector(*this); }
		bool remove(const T& value) { return remove(*this, value); }
		void removeAt(i64 pos) { this->erase(this->begin() + pos); }

#pragma region static implementations

		static bool contains(const std::vector<T>& vector, const T& value)
		{
			return std::find(vector.begin(), vector.end(), value) != vector.end();
		}

		static i64 indexOf(const std::vector<T>& vector, const T& value)
		{
			auto beg = vector.begin();
			auto idx = std::find(vector.begin(), vector.end(), value);
			return idx == vector.end() ? -1 :  idx - beg;
		}

		static bool any(const std::vector<T>& vector, std::function<bool(const T&)> condition)
		{
			cauto& end = vector.end();
			return std::any_of(vector.begin(), end, condition);
		}

		static bool all(const std::vector<T>& vector, std::function<bool(const T&)> condition)
		{
			auto& end = vector.end();
			return std::all_of(vector.begin(), end, condition);
		}

		static bool remove(std::vector<T>& vector, const T& value)
		{
			auto end = vector.end();
			auto pos = std::find(vector.begin(), end, value);
			if (pos != end)
			{
				vector.erase(pos);
				return true;
			}
			return false;
		}
#pragma endregion 
	};
}
