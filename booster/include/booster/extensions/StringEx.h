#pragma once

#include "BaseEx.h"
#include "../types.h"
#include "../linq/Enumerable.h"

namespace Booster
{		
	class StringEx : public BaseEx<std::string>
	{
	public:
		StringEx() { }
		StringEx(const std::string& str) :
			BaseEx(str) { }
		StringEx(const char* str) :
			BaseEx(str) { }

#if !defined(__clang__) && !defined(__GNUC__)
		operator std::string() const { return *this; }
#endif

		bool some() const { return some(*this); }
		//StringEx format(...) const;
		StringEx join(const Enumerable<std::string>& ie) const;

		template<typename... T>
		StringEx join(const T...items) const
		{
			std::vector<std::string> vvalues = { items... };
			return join(vvalues);
		}

		StringEx toLower() const { return toLower(*this); }
		StringEx toUpper() const { return toUpper(*this); }


		bool equals(const std::string& other, bool no_case = false) const { return equals(*this, other, no_case); }
		bool equalsNoCase(const std::string& other) const { return equalsNoCase(*this, other); }

		StringEx trim(const std::string& chars = " \t\r\n") const { return trim(*this, chars); }
		StringEx trimStart(const std::string& chars = " \t\r\n") const { return trimStart(*this, chars); }
		StringEx trimEnd(const std::string& chars = " \t\r\n") const { return trimEnd(*this, chars); }
		StringEx replace(const std::string& from, const std::string& with) const { return replace(*this, from, with); }

		i64 indexOf(const std::string& value, size_t pos = 0) const { return find(value, pos); }
		i64 indexOf(char c, size_t pos = 0) const { return find(c, pos); }
		i64 lastIndexOf(const std::string& value, size_t pos = npos) const { return rfind(value, pos); }
		i64 lastIndexOf(char c, size_t pos = npos) const { return rfind(c, pos); }

		Enumerable<StringEx> split(const std::string& separator, bool remove_empty = false) const { return split(*this, separator, remove_empty); }
		Enumerable<StringEx> split(char separator, bool remove_empty = false) const { return split(*this, separator, remove_empty); }

		std::wstring toWString() const;

		bool operator == (const char* c) const;
#pragma region static implementations
		static bool some(const std::string& str);

		static std::string trim(const std::string& str, const std::string& chars);
		static std::string trimStart(const std::string& str, const std::string& chars);
		static std::string trimEnd(const std::string& str, const std::string& chars);
		static std::string replace(const std::string& str, const std::string& from, const std::string& with);

		/*static i64 indexOf(std::string str, const std::string& value, size_t pos = 0);
		static i64 indexOf(std::string str, char c, size_t pos = 0);
		static i64 lastIndexOf(std::string str, const std::string& value, size_t pos = npos);
		static i64 lastIndexOf(std::string str, char c, size_t pos = npos);*/

		//static bool startsWith(std::string str, const std::string& value, bool no_case = false);
		//static bool endsWith(std::string str, const std::string& value, bool no_case = false);

		static Enumerable<StringEx> split(std::string str, const std::string& separator, bool remove_empty = false);
		static Enumerable<StringEx> split(std::string str, char separator, bool remove_empty = false);

		static std::string toLower(std::string str);
		static std::string toUpper(std::string str);

		static bool equals(const std::string& a, const std::string& b, bool no_case = false);
		static bool equalsNoCase(const std::string& a, const std::string& b);

#pragma endregion 
	};

}
