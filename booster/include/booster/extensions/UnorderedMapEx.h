#pragma once

#include "BaseEx.h"
#include "DictMethods.h"

namespace Booster
{
	template<typename TKey, typename TValue>
	class UnorderedMapEx : 
		public BaseEx<std::unordered_map<TKey, TValue>>, 
		public DictMethods<TKey, TValue, UnorderedMapEx<TKey, TValue>>
	{
		class enumerator : public IEnumerator<std::pair<TKey, TValue >>
		{
			typename std::unordered_map<TKey, TValue>::iterator _begin;
			typename std::unordered_map<TKey, TValue>::iterator _it;
			typename std::unordered_map<TKey, TValue>::iterator _end;
			std::pair<TKey, TValue> _current;
		public:
			enumerator(UnorderedMapEx<TKey, TValue >& dict) : 
				_begin(dict.begin()),
				_end(dict.end())
			{
			}

			const std::pair<TKey, TValue>& current() const override
			{
				return _current;
			}
			bool moveNext() override
			{
				if(_it != _end)
				{
					_current = std::make_pair(_it->first, _it->second);
					++_it;
					return true;
				}
				return false;
			}
			void reset() override
			{
				_it = _begin;
			}
		};

	public:
		UnorderedMapEx() {}
		UnorderedMapEx(const std::unordered_map<TKey, TValue>& proto) : BaseEx<std::unordered_map<TKey, TValue>>(proto) {}
		UnorderedMapEx(const std::initializer_list<std::pair<TKey, TValue>>& init)
		{
			this->addRange(init);
		}

		std::shared_ptr<IEnumerator<std::pair<TKey, TValue>>> getEnumerator() const
		{
			return std::make_shared<enumerator>(const_cast<UnorderedMapEx<TKey, TValue>&>(*this));
		}

		operator std::unordered_map<TKey, TValue>() const { return *this; }
		operator Enumerable<std::pair<TKey, TValue>>() const { return Enumerable<std::pair<TKey, TValue>>([=]{ return getEnumerator(); }); }

#pragma region IEnumerable<T>

		bool any(std::function<bool(const std::pair<TKey, TValue>&)> condition) const { return any(*this, condition); }
		bool all(std::function<bool(const std::pair<TKey, TValue>&)> condition) const { return all(*this, condition); }


#pragma endregion 


#pragma region static implementations

		static bool any(const std::unordered_map<TKey, TValue>& vector, std::function<bool(const std::pair<TKey, TValue>&)> condition)
		{
			auto& end = vector.end();
			return std::any_of(vector.begin(), end, condition);
		}

		static bool all(const std::unordered_map<TKey, TValue>& vector, std::function<bool(const std::pair<TKey, TValue>&)> condition)
		{
			auto& end = vector.end();
			return std::all_of(vector.begin(), end, condition);
		}
#pragma endregion 
	};
}
