#pragma once

namespace Booster
{
	template<typename T>
	class BaseEx : public T
	{
	protected:
		BaseEx()
		{
		}
		BaseEx(const T& copy) : 
			T(copy)
		{
		}
	public:
		T& toTarget() const
		{
			// ReSharper disable once CppCStyleCast
			return (T&)*this;
		}
	};
}
