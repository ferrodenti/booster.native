#pragma once

namespace Booster
{
	class NoCopy
	{
	public:
		NoCopy() = default;
		virtual ~NoCopy() = default;

	private:
		NoCopy(NoCopy&& ) = delete;
		NoCopy(const NoCopy&) = default;
		NoCopy& operator=(const NoCopy&) = delete;
		NoCopy& operator=(const NoCopy&&) = delete;
	};
}