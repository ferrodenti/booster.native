#pragma once

namespace Booster
{
	class Trace
	{
		std::stringstream _stream;
	public:
		~Trace();

		template<typename T>
		void write(const T& obj)
		{
			_stream << obj;
		}

		void write(const Timepoint& ts);
		void write(const std::exception& ex);
	};

	class NoTrace
	{
	public:
		template<typename T>
		NoTrace operator << (const T& obj)
		{
			return NoTrace();
		}
	};

	template<typename T>
	SP<Trace> operator << (SP<Trace> trace, const T& obj)
	{
		trace->write(obj);
		return trace;
	}
}


#ifndef BOOSTER_NO_TRACE
#	ifdef _DEBUG
#		define TRACE std::make_shared<Trace>()
#	else
#		define TRACE NoTrace()
#	endif
#endif