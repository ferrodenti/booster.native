#pragma once

#ifndef BOOSTER_EXTENSIONS_OP
#define BOOSTER_EXTENSIONS_OP $
#endif

#include "extensions/VectorEx.h"
#include "extensions/StringEx.h"
#include "extensions/UnorderedMapEx.h"
#include "extensions/MapEx.h"
#include "JoinStream.h"

namespace Booster
{
#pragma region StringEx
	inline StringEx& BOOSTER_EXTENSIONS_OP(std::string& str)
	{
		return reinterpret_cast<StringEx&>(str);
	}
	inline const StringEx& BOOSTER_EXTENSIONS_OP(const std::string& str)
	{
		return reinterpret_cast<const StringEx&>(str);
	}
	inline StringEx& BOOSTER_EXTENSIONS_OP(StringEx& str)
	{
		return str;
	}
	inline StringEx BOOSTER_EXTENSIONS_OP(const char* str)
	{
		return StringEx(str);
	}
#pragma endregion 

#pragma region VectorEx
	template<typename T>
	VectorEx<T>& BOOSTER_EXTENSIONS_OP(std::vector<T>& vect)
	{
		return reinterpret_cast<VectorEx<T>&>(vect);
	}
	template<typename T>
	const VectorEx<T>& BOOSTER_EXTENSIONS_OP(const std::vector<T>& vect)
	{
		return reinterpret_cast<const VectorEx<T>&>(vect);
	}
#pragma endregion 
	template<typename T>
	VectorEx<T> BOOSTER_EXTENSIONS_OP(const std::initializer_list<T>& list)
	{
		return VectorEx<T>(list);
	}

#pragma region UnorderedMapEx
	template<typename TKey, typename TValue>
	UnorderedMapEx<TKey, TValue>& BOOSTER_EXTENSIONS_OP(std::unordered_map<TKey, TValue>& dict)
	{
		return reinterpret_cast<UnorderedMapEx<TKey, TValue>&>(dict);
	}

	template<typename TKey, typename TValue>
	const UnorderedMapEx<TKey, TValue>& BOOSTER_EXTENSIONS_OP(const std::unordered_map<TKey, TValue>& dict)
	{
		return reinterpret_cast<const UnorderedMapEx<TKey, TValue>&>(dict);
	}
#pragma endregion 

#pragma region MapEx
	template<typename TKey, typename TValue>
	MapEx<TKey, TValue>& BOOSTER_EXTENSIONS_OP(std::map<TKey, TValue>& dict)
	{
		return reinterpret_cast<MapEx<TKey, TValue>&>(dict);
	}

	template<typename TKey, typename TValue>
	const MapEx<TKey, TValue>& BOOSTER_EXTENSIONS_OP(const std::map<TKey, TValue>& dict)
	{
		return reinterpret_cast<const MapEx<TKey, TValue>&>(dict);
	}
#pragma endregion 

	inline JoinStream BOOSTER_EXTENSIONS_OP()
	{
		return move(JoinStream());
	}
}
