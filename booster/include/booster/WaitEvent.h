#pragma once

#include <booster/NoCopy.h>

namespace Booster
{
	class WaitEvent : NoCopy
	{
	public:
		WaitEvent();
		explicit WaitEvent(bool initial);

		void set();
		void setAll();
		void reset();
		void resetAll();
		bool waitOne(int interval = -1);

	private:
		std::atomic<bool> _flag;
		std::atomic<bool> _flagAll;
		std::mutex _protect;
		std::condition_variable _signal;
	};
}
