#pragma once
#include <booster/NoCopy.h>

namespace Booster
{
	class IndexedDirectory : NoCopy
	{
		std::string _key;
		std::string _directory;
		std::string _extension;
		std::fstream _seq_stream;

		std::atomic<i64> _sequence;

		mutable std::mutex _lock;
		static std::mutex _cache_lock;

		static std::unordered_map<std::string, SP<IndexedDirectory>> _cache; //TODO: weak ptr
		i64 initSequenceNoLock();

	public:
		IndexedDirectory(const std::string& key, const std::string& directory, const std::string& extension);

		std::string seq_filename;

		i64 initSequence();
		std::string getNewFilename(bool check_exist = true);
		void dispose();

		static SP<IndexedDirectory> get(const std::string& path);
		static SP<IndexedDirectory> get(const std::string& directory, const std::string& extension);
		static bool remove(SP<IndexedDirectory> dir);
	};
}
