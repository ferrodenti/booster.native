#pragma once
#include "IEnumerator.h"

namespace Booster
{
	template<typename T, typename TPos>
	class YieldState
	{
	public:
		T value;
		TPos position;
		bool done = false;
		std::function<bool(YieldState<T, TPos>&)>* continue_with = nullptr;
	};

	template<typename T, typename TPos>
	class YieldEnumerator : public IEnumerator<T>
	{
		std::function<void(YieldState<T, TPos>&)> _proc;
		YieldState<T, TPos> _state;
		TPos _initial;
	public:
		YieldEnumerator(std::function<void(YieldState<T, TPos>&)> proc, TPos initial) :
			_proc(proc),
			_state(),
			_initial(initial)
		{
		}

		const T& current() const override
		{
			return _state.value;
		}

		bool moveNext() override
		{
			if (_state.done)
				return false;
			
			if(_state.continue_with)
				(*_state.continue_with)(_state);
			else
				_proc(_state);

			return true;
		}

		void reset() override
		{
			_state.done = false;
			_state.position = _initial;
			_state.continue_with = nullptr;
		}
	};
}
