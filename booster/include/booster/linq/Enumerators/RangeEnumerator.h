#pragma once
#include "IEnumerator.h"

namespace Booster
{
	template<typename T>
	class RangeEnumerator : public IEnumerator<T>
	{
		std::shared_ptr<IEnumerator<T>> _parent;
		i64 _skip;
		i64 _take;
		i64 _count = 0;

	public:
		RangeEnumerator(std::shared_ptr<IEnumerator<T>> parent, i64 skip, i64 take) :
			_parent(parent),
			_skip(skip),
			_take(take)
		{
		}

		T& current() const override
		{
			return _parent->current();
		}

		bool moveNext() override
		{
			for(auto i=0; i<_skip; ++i)
				if(!_parent->moveNext())
					return false;

			if(_take < 0 || _count++ < _take)
				return _parent->moveNext();

			return false;
		}

		void reset() override
		{
			_parent->reset();
			_count = 0;
		}
	};
}
