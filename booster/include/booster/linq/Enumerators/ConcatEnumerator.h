#pragma once
#include "IEnumerator.h"

namespace Booster
{
	template<typename T>
	class ConcatEnumerator : public IEnumerator<T>
	{
		std::vector<std::shared_ptr<IEnumerator<T>>> _enumarators;
		size_t _idx = 0;

	public:
		ConcatEnumerator(const std::initializer_list<std::shared_ptr<IEnumerator<T>>>& enumarators) :
			_enumarators(enumarators)
		{
		}

		ConcatEnumerator(const std::vector<std::shared_ptr<IEnumerator<T>>>& enumarators) :
			_enumarators(enumarators)
		{
		}

		const T& current() const override
		{
			return _enumarators[_idx]->current();
		}

		bool moveNext() override
		{
			while(!_enumarators[_idx]->moveNext())
			{
				if(++_idx >= _enumarators.size())
					return false;

				_enumarators[_idx]->reset();
			}
			return true;
		}

		void reset() override
		{
			_idx = 0;
			_enumarators[_idx]->reset();
		}
	};
}
