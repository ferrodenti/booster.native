#pragma once

namespace Booster
{
	template<typename T>
	class IEnumerator
	{
	public:
		virtual ~IEnumerator() = default;

		virtual const T& current() const = 0;
		virtual bool moveNext() = 0;
		virtual void reset() = 0;
	};
}