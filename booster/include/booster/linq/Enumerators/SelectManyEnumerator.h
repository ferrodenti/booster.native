#pragma once
#include "IEnumerator.h"

namespace Booster
{
	template<typename T1, typename T2>
	class SelectManyEnumerator : public IEnumerator<T2>
	{
		std::shared_ptr<IEnumerator<T1>> _parent;
		std::shared_ptr<IEnumerator<T2>> _current;
		std::function<std::shared_ptr<IEnumerator<T2>>(const T1&)> _selector;

	public:
		SelectManyEnumerator(std::shared_ptr<IEnumerator<T1>> parent, std::function<std::shared_ptr<IEnumerator<T2>>(const T1&)> selector) :
			_parent(parent),
			_selector(selector)
		{
		}

		const T2& current() const override
		{
			return _current->current();
		}

		bool moveNext() override
		{
			while(!_current || !_current->moveNext())
			{
				if(!_parent->moveNext())
					return false;

				_current = _selector(_parent->current());
				_current->reset();

				if(_current->moveNext())
					return true;

				_current = nullptr;
			}
			return true;
		}

		void reset() override
		{
			_parent->reset();
			_current = nullptr;
		}
	};
}
