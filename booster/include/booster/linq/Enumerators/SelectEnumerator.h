#pragma once
#include "IEnumerator.h"

namespace Booster
{
	template<typename T1, typename T2>
	class SelectEnumerator : public IEnumerator<T2>
	{
		std::shared_ptr<IEnumerator<T1>> _parent;
		std::function<const T2(const T1&)> _selector;
		mutable T2 _current;
	public:
		SelectEnumerator(std::shared_ptr<IEnumerator<T1>> parent, std::function<const T2(const T1&)> selector) :
			_parent(parent),
			_selector(selector)
		{
		}

		const T2& current() const override
		{
			auto& c = _parent->current();
			_current = _selector(c);
			return _current;
		}

		bool moveNext() override
		{
			return _parent->moveNext();
		}

		void reset() override
		{
			_parent->reset();
		}
	};
}
