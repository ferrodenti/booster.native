#pragma once
#include "IEnumerator.h"

namespace Booster
{
	template<typename T>
	class WhereEnumerator : public IEnumerator<T>
	{
		std::shared_ptr<IEnumerator<T>> _parent;
		std::function<bool(const T&)> _predicate;

	public:
		WhereEnumerator(std::shared_ptr<IEnumerator<T>> parent, std::function<bool(const T&)> predicate) :
			_parent(parent),
			_predicate(predicate)
		{
		}

		T& current() const override
		{
			return _parent->current();
		}

		bool moveNext() override
		{
			while(_parent->moveNext())
			{
				if(_predicate(_parent->current()))
					return true;
			}
			return false;
		}

		void reset() override
		{
			_parent->reset();
		}
	};
}
