#pragma once
#include "IEnumerator.h"

namespace Booster
{
	template<typename T>
	class ConstForwardIterator
	{
		std::shared_ptr<IEnumerator<T>> _enumerator;
		bool _is_end = false;

	public:
		ConstForwardIterator() :
			_is_end(true)
		{
		}
		ConstForwardIterator(std::shared_ptr<IEnumerator<T>> enumerator) :
			_enumerator(enumerator)
		{
			_enumerator->reset();
			_is_end = !_enumerator->moveNext();
		}

		const T& operator*() const
		{
			return _enumerator->current();
		}
		T* operator->() const
		{
			return const_cast<T*>(&_enumerator->current());
		}

		ConstForwardIterator& operator++()
		{
			_is_end = !_enumerator->moveNext();
			return *this;
		}

		bool operator!=(const ConstForwardIterator& other) const
		{
			return _is_end != other._is_end;
		}
	};
}
