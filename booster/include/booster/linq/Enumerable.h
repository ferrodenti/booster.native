#pragma once
#include "Enumerators/ConstForwardIterator.h"
#include "LinqMethods.h"

namespace Booster
{		
	template<typename T>
	class Enumerable : public LinqMethods<T, Enumerable<T>>
	{
		std::function<std::shared_ptr<IEnumerator<T>>()> _enumerator_creator;
	public:
		Enumerable(std::function<std::shared_ptr<IEnumerator<T>>()> enumerator_creator) :
			_enumerator_creator(enumerator_creator)
		{
		}

		Enumerable(std::initializer_list<T> list)
		{
			std::vector<T> v(list);
			_enumerator_creator = [v] { return $(v).getEnumerator(); };
		}
		
		std::shared_ptr<IEnumerator<T>> getEnumerator() const
		{
			return _enumerator_creator();
		}

		ConstForwardIterator<T> begin() const
		{
			return ConstForwardIterator<T>(getEnumerator());
		}
		ConstForwardIterator<T> end() const
		{
			return ConstForwardIterator<T>();
		}

		static Enumerable<T> getEmpty()
		{
			return $(std::vector<T>());
		}
	};
}
