#pragma once
#include "Enumerators/WhereEnumerator.h"
#include "Enumerators/SelectEnumerator.h"
#include "Enumerators/SelectManyEnumerator.h"
#include "Enumerators/RangeEnumerator.h"
#include "Enumerators/YieldEnumerator.h"
#include "Enumerators/ConcatEnumerator.h"

namespace Booster
{
	template<typename T>
	class VectorEx;

	template<typename T, typename TThis>
	class LinqMethods
	{
		std::shared_ptr<IEnumerator<T>> getEnumerator() const
		{
			// ReSharper disable once CppCStyleCast
			return ((TThis*)this)->getEnumerator();
		}

	public:

		const T& first() const
		{
			auto e = getEnumerator();
			e->reset();
			if (e->moveNext())
				return e->current();

			throw std::logic_error("The source sequence is empty");
		}

		T firstOrDefault() const
		{
			auto e = getEnumerator();
			e->reset();
			if (e->moveNext())
				return e->current();

			return T();
		}

		const T& firstOrDefault(T& default_) const
		{
			auto e = getEnumerator();
			e->reset();
			if (e->moveNext())
				return e->current();

			return default_;
		}

		bool any(std::function<bool(const T&)> condition) const
		{
			auto e = getEnumerator();
			e->reset();

			while (e->moveNext())
				if (condition(e->current()))
					return true;

			return false;
		}

		bool all(std::function<bool(const T&)> condition) const
		{
			auto e = getEnumerator();
			e->reset();

			while (e->moveNext())
				if (!condition(e->current()))
					return false;

			return true;
		}

		template<typename... T2>
		bool containsAny(const T2...values) const
		{
			std::vector<T> vvalues = { values... };
			auto beg = vvalues.begin();
			auto end = vvalues.end();
			return any([=](const T& item) { return std::find(beg, end, item) != end; });
		}

		bool contains(const T& value) const
		{
			auto e = getEnumerator();
			e->reset();

			while (e->moveNext())
				if (e->current() == value)
					return true;

			return false;
		}

		i64 indexOf(const T& value) const
		{
			i64 idx = 0;
			auto e = getEnumerator();
			e->reset();

			while (e->moveNext())
			{
				if (e->current() == value)
					return idx;

				++idx;
			}
			return -1;
		}

		i64 indexOf(std::function<bool(const T&)> condition) const
		{
			i64 idx = 0;

			auto e = getEnumerator();
			e->reset();
			while (e->moveNext())
			{
				if (condition(e->current()))
					return idx;

				++idx;
			}
			return -1;
		}

		Enumerable<T> concat(const Enumerable<T>& other) const
		{
			auto ie1 = getEnumerator();
			auto ie2 = other.getEnumerator();

			return Enumerable<T>([=]
			{
				return std::make_shared<ConcatEnumerator<T>>(std::vector<std::shared_ptr<IEnumerator<T>>>({ ie1, ie2 }));
			});
		}

		template<typename... T2>
		Enumerable<T> concat(const Enumerable<T>& other, T2...rest) const
		{
			std::vector<Enumerable<T>> v = { rest ... };
			std::vector<std::shared_ptr<IEnumerator<T>>> ies = { getEnumerator(), other.getEnumerator() };

			for(auto ie : v)
				ies.push_back(ie.getEnumenator());

			return Enumerable<T>([=]
			{
				return std::make_shared<ConcatEnumerator<T>>(ies);
			});
		}

		template<typename T2>
		Enumerable<T2> select(std::function<T2(const T&)> selector) const
		{
			auto ie = getEnumerator();
			return Enumerable<T2>([=]
			{
				return std::make_shared<SelectEnumerator<T, T2>>(ie, selector);
			});
		}

		template<typename T2>
		Enumerable<T2> cast() const
		{
			// ReSharper disable once CppCStyleCast
			return select<T2>([](const T& i){ return (T2)i; });
		}

		template<typename T2>
		Enumerable<T2> selectMany(std::function<const Enumerable<T2>(const T&)> selector) const
		{
			auto ie = getEnumerator();
			return Enumerable<T2>([=]
			{
				return std::make_shared<SelectManyEnumerator<T, T2>>(ie, [=](T& t) { return selector(t).getEnumerator(); });
			});
		}

		template<typename TOrderGetter>
		VectorEx<T> orderBy(TOrderGetter getter)
		{
			auto v = toVector();
			std::sort(v.begin(), v.end(), [=](T& a, T& b) { return getter(a) < getter(b); });
			return reinterpret_cast<VectorEx<T>&>(v);
		}

		template<typename TOrderGetter>
		VectorEx<T> orderByDesc(TOrderGetter getter)
		{
			auto v = toVector();
			std::sort(v.begin(), v.end(), [=](T& a, T& b) { return getter(a) > getter(b); });
			return reinterpret_cast<VectorEx<T>&>(v);
		}

		VectorEx<T> sort()
		{
			return orderBy([](T& i) {return i; });
		}
		VectorEx<T> sortDesc()
		{
			return orderByDesc([](T& i) {return i; });
		}

		Enumerable<T> range(i64 skip, i64 take) const
		{
			auto ie = getEnumerator();
			return Enumerable<T>([=]
			{
				return std::make_shared<RangeEnumerator<T>>(ie, skip, take);
			});
		}

		Enumerable<T> skip(i64 number) const
		{
			auto ie = getEnumerator();
			return Enumerable<T>([=]
			{
				return std::make_shared<RangeEnumerator<T>>(ie, number, -1);
			});
		}

		Enumerable<T> take(i64 number) const
		{
			auto ie = getEnumerator();
			return Enumerable<T>([=]
			{
				return std::make_shared<RangeEnumerator<T>>(ie, 0, number);
			});
		}

		Enumerable<T> where(std::function<bool(const T&)> condition) const
		{
			auto ie = getEnumerator();
			return Enumerable<T>([=]
			{
				return std::make_shared<WhereEnumerator<T>>(ie, condition);
			});
		}

		Enumerable<T> whereSome() const
		{
			return where([](T& i){ if(i) return true; return false; });
		}

		std::vector<T> toVector() const
		{
			std::vector<T> res;
			pushToVector(res);
			return res;
		}

		i64 pushToVector(std::vector<T>& vector) const
		{
			i64 res = 0;
			// ReSharper disable once CppCStyleCast
			auto ie = ((TThis*)this);
			for (auto it = ie->begin(); it != ie->end(); ++it, ++res)
				vector.push_back(*it);

			return res;
		}

		template<typename T2>
		static Enumerable<T> yield(std::function<void(YieldState<T, T2>&)> proc, T2 initial = T2())
		{
			return Enumerable<T>([=]
			{
				return std::make_shared<YieldEnumerator<T, T2>>(proc, initial);
			});
		}

		bool equals(const Enumerable<T>& other)
		{
			auto ie1 = getEnumerator();
			auto ie2 = other.getEnumerator();
			ie1->reset();
			ie2->reset();

			while (true)
			{
				bool nxt = ie1->moveNext();
				if(nxt != ie2->moveNext())
					return false;
				if(!nxt)
					return true;

				if(ie1->current() != ie2->current())
					return false;
			}
		}

		bool equals(std::initializer_list<T> other)
		{
			return equals(VectorEx<T>(other));
		}
	};
}
