#pragma once

namespace Booster
{
	class RetryEnv
	{
	public:
		template<typename T>
		static T Do(std::function<T()> proc, int retries = 5, int interval = 500, bool no_throw = false, T def = T())
		{
			if(retries <= 0)
				return proc();

			std::exception exception;
			for(int i=0;; ++i)
			{
				try
				{
					return proc();
				}
				catch(...)
				{
					if(i>= retries)
					{
						if(!no_throw)
							throw;

						return def;
					}
					std::this_thread::sleep_for(std::chrono::milliseconds(interval));
				}
			}
		}

		static void Do(std::function<void()> proc, int retries = 5, int interval = 500, bool no_throw = false)
		{
			if (retries <= 0)
				return proc();

			std::exception exception;
			for (int i = 0;; ++i)
			{
				try
				{
					 proc();
					 return;
				}
				catch (...)
				{
					if (i >= retries)
					{
						if (!no_throw)
							throw;

						return;
					}
					//std::this_thread::sleep_for(milliseconds(interval));
				}
			}
		}
	};
}
