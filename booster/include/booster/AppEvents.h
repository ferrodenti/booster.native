#pragma once

#include "events.h"

namespace Booster
{
	class Log;
	class AppEvents
	{
		static AppEvents _instance;
		static void onQuit();
		static void onSEHException(const std::exception& ex);
		static void handleQuitEvent();
		static void handleSEHExceptions();
	public:
		static Log* log;

		static void setup(bool disable_error_dialog = true, bool handle_seh_exceptions = true, bool handle_quit_event = true);
		static void disableConsoleQuickEdit();

		static Event<AppEvents> quit;
		static Event<AppEvents, const std::exception&> seh_exception;
	};
}