#pragma once


namespace Booster
{
	template<typename TItem, typename TPriority = float>
	class PriorityQueue
	{
		i64 _capacity;
		std::vector<TItem> _items;
		std::vector<TPriority> _priorities;
		std::shared_mutex _lock;

		size_t findIndexFor(const TPriority& priority) const
		{
			size_t len = _priorities.size();
			size_t beg = 0;

			while(true)
			{
				if (len == 0)
					return beg;

				size_t mid = len / 2;
				TPriority midv = _priorities[beg + mid];

				if(midv > priority)
					len = mid;
				else
				{
					++mid;
					beg += mid;
					len -= mid;	
				}
			}
		}
		TPriority _initial_max;
	public:
		TPriority max;

		PriorityQueue(i64 capacity = -1, TPriority max = std::numeric_limits<TPriority>::max()) :
			_capacity(capacity),
			_initial_max(max),
			max(max)
		{
			if(_capacity > 0)
			{
				_items.reserve(_capacity);
				_priorities.reserve(_capacity);
			}
		}

		TPriority min() const { return empty() ? TPriority() : _priorities[0]; }
		bool empty() const  { return _items.empty(); }
		size_t size() const { return _items.size(); }
		typename std::vector<TItem>::iterator begin()				{	return _items.begin();	}
		typename std::vector<TItem>::iterator end()				{	return _items.end();	}
		typename std::vector<TItem>::const_iterator begin() const 	{	return _items.begin();	}
		typename std::vector<TItem>::const_iterator end() const	{	return _items.end();	}

		void clear()
		{
			write_lock _(_lock);
			_items.clear();
			_priorities.clear();

			if (_capacity > 0)
				max = _initial_max;
		}

		bool tryEnqueue(const TItem item, const TPriority priority)
		{
			if(_capacity > 0 && max < priority)
				return false;

			write_lock _(_lock);

			auto i = findIndexFor(priority);
			_items.insert(_items.begin() + i, item);
			_priorities.insert(_priorities.begin() + i, priority);

			i = _items.size();
			if(_capacity > 0 && i > size_t(_capacity))
			{
				--i;
				_items.erase(_items.begin() + i);
				_priorities.erase(_priorities.begin() + i);
				max = _priorities[i-1];
			}
			return true;
		}

		bool removeAt(u64 position)
		{
			write_lock _(_lock);

			if (position >= size())
				return false;
			_items.erase(_items.begin() + position);
			_priorities.erase(_priorities.begin() + position);

			if (_capacity > 0)
				max = _initial_max; // мы что-то удалили, поэтому есть место для любых элементов

			return true;
		}

		bool remove(const TPriority& priority)
		{
			write_lock _(_lock);

			auto ibeg = _items.begin();
			auto pbeg = _priorities.begin();
			auto idx = i64(findIndexFor(priority));
			bool result = false;

			for (i64 i = std::min(idx, i64(_items.size()) - 1); i >= 0; --i)
			{
				if (_priorities[i] != priority)
					break;

				_items.erase(ibeg + i);
				_priorities.erase(pbeg + i);
				idx--;
				result = true;
			}
			idx++;
			while (idx < i64(_items.size()))
			{
				if (_priorities[idx] != priority)
					break;

				_items.erase(ibeg + idx);
				_priorities.erase(pbeg + idx);
				result = true;
			}

			if (result && _capacity > 0)
				max = _initial_max; // мы что-то удалили, поэтому есть место для любых элементов

			return result;
		}

		bool remove(const TItem& item, const TPriority& priority)
		{
			write_lock _(_lock);

			auto ibeg = _items.begin();
			auto pbeg = _priorities.begin();
			auto idx = i64(findIndexFor(priority));
			bool result = false;

			for (i64 i = std::min(idx, i64(_items.size()) - 1); i >= 0; --i)
			{
				if (_priorities[i] != priority || _items[i] != item)
					break;

				_items.erase(ibeg + i);
				_priorities.erase(pbeg + i);
				idx--;
				result = true;
			}
			idx++;
			while(idx < i64(_items.size()))
			{
				if (_priorities[idx] != priority || _items[idx] != item)
					break;

				_items.erase(ibeg + idx);
				_priorities.erase(pbeg + idx);
				result = true;
			}

			if(result && _capacity > 0)
				max = _initial_max; // мы что-то удалили, поэтому есть место для любых элементов

			return result;
		}

		bool tryPeek(TItem& item, TPriority& priority, u64 position = 0)
		{
			read_lock _(_lock);

			if(position >= size())
				return false;

			item = _items[position];
			priority = _priorities[position];
			return true;
		}

		bool tryDequeue(TItem& item, TPriority& priority)
		{
			write_lock _(_lock);

			if(_items.empty())
				return false;

			item = _items[0];
			priority = _priorities[0];
			_items.erase(_items.begin());
			_priorities.erase(_priorities.begin());

			if(_capacity > 0)
				max = _initial_max; // мы что-то удалили, поэтому есть место для любых элементов

			return true;
		}

		int toVector(std::vector<TItem>& results)
		{
			read_lock _(_lock);

			for (auto& item : _items)
				results.push_back(item);

			return results.size();
		}
		int toVector(std::vector<TItem>& results, int count)
		{
			read_lock _(_lock);

			for (auto& item : _items)
			{
				if (count-- <= 0)
					break;

				results.push_back(item);
			}

			return results.size();
		}
	};
}