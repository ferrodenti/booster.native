#pragma once

#include "PriorityQueue.h"
#include <future>

namespace Booster
{	
	class TaskSchedule 
	{	
	public:
		class Task;
	private:
		class TaskImpl
		{
			TaskSchedule& _owner;
			std::function<void(Task&)> _action;
			bool _inclusive;
		public:
			Timepoint time;
			u64 period_ms;
			int changed = 0;
			bool canceled = false;
			bool done = false;
			bool executing = false;
			bool not_done = false;
			TaskImpl(TaskSchedule& owner, Timepoint time, u64 period_ms, std::function<void(Task&)> action, bool inclusive);
			void exe(SP<TaskImpl> self);
			void cancel(SP<TaskImpl> self);
			void change(SP<TaskImpl> self, Timepoint time);
		};
		std::promise<void> _exit_signal;
		std::atomic<bool> _has_thread = false;
		std::thread _thread;
		mutable std::mutex _thread_lock;
		mutable std::mutex _exe_lock;
		PriorityQueue<SP<TaskImpl>, Timepoint> _queue;

		void proc();
		void plan(SP<TaskImpl> task);

	public:
		~TaskSchedule();

		class Task
		{
			SP<TaskImpl> _impl;
		public:
			explicit Task(SP<TaskImpl> impl);

			Timepoint time() const;

			bool cancel() const;
			bool canceled() const;
			bool done() const;
			bool doneOrCanceled() const;

			void setPeriod(i64 period_ms) const;
			bool delay(Timepoint time) const;
			bool delay(i64 milliseconds) const;

			template<class _Rep, class _Period>
			Task delay(const std::chrono::duration<_Rep, _Period>& rel_time)
			{
				return delay(std::chrono::system_clock::now() + rel_time);
			}
		};

		SP<std::lock_guard<std::mutex>> executionLock() const;

		Task planSingle(i64 milliseconds, std::function<void()> action);
		Task planSingle(Timepoint time, std::function<void()> action);
		Task planRepeating(u64 milliseconds, std::function<void()> action, bool inclusive = true);
		Task plan(Timepoint time, u64 period_ms, std::function<void()> action, bool inclusive = true);

		Task planSingle(i64 milliseconds, std::function<void(Task&)> action);
		Task planSingle(Timepoint time, std::function<void(Task&)> action);
		Task planRepeating(u64 milliseconds, std::function<void(Task&)> action, bool inclusive = true);
		Task plan(Timepoint time, u64 period_ms, std::function<void(Task&)> action, bool inclusive = true);

		template<class _Rep, class _Period>
		Task planSingle(const std::chrono::duration<_Rep, _Period>& rel_time, std::function<void()> action)
		{
			return planSingle(std::chrono::system_clock::now() + rel_time, action);
		}
		template<class _Rep, class _Period>
		Task planRepeating(const std::chrono::duration<_Rep, _Period>& period, std::function<void()> action, bool inclusive = true)
		{
			return planRepeating(std::chrono::duration_cast<std::chrono::milliseconds>(period).count(), action, inclusive);
		}
		template<class _Rep, class _Period>
		Task plan(Timepoint time, const std::chrono::duration<_Rep, _Period>& period, std::function<void()> action, bool inclusive = true)
		{
			return plan(time, std::chrono::duration_cast<std::chrono::milliseconds>(period).count(), action, inclusive);
		}

		template<class _Rep, class _Period>
		Task planSingle(const std::chrono::duration<_Rep, _Period>& rel_time, std::function<void(Task&)> action)
		{
			return planSingle(std::chrono::system_clock::now() + rel_time, action);
		}
		template<class _Rep, class _Period>
		Task planRepeating(const std::chrono::duration<_Rep, _Period>& period, std::function<void(Task&)> action, bool inclusive = true)
		{
			return planRepeating(std::chrono::duration_cast<std::chrono::milliseconds>(period).count(), action, inclusive);
		}
		template<class _Rep, class _Period>
		Task plan(Timepoint time, const std::chrono::duration<_Rep, _Period>& period, std::function<void(Task&)> action, bool inclusive = true)
		{
			return plan(time, std::chrono::duration_cast<std::chrono::milliseconds>(period).count(), action, inclusive);
		}
	};
}
