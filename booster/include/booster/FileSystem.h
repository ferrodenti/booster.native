#pragma once

namespace Booster
{
	class FileSystem
	{
	public:
		static std::string currentDir();
		static bool exists(const std::string& fileName);
		static bool ensurePath(const std::string& path);
		static bool remove(const std::string& fileName);

		static std::string ensureDirectoryEnding(const std::string& path);

		static std::string getFolderName(const std::string& path);
		static std::string getFileName(const std::string& path);

		static std::fstream openOrCreate(const std::string& fileName, std::ios_base::openmode open_mode);
		static std::fstream openOrCreate(const std::string& fileName, std::ios_base::openmode open_mode, bool& created);
		static bool openOrCreate(const std::string& fileName, std::ios_base::openmode open_mode, std::fstream& result);

		static void open(const std::string& fileName, std::ios_base::openmode open_mode, std::fstream& result);

		static std::string readAll(const std::string& fileName, bool binary = true);
		static std::string readAll(std::istream& stream);
		static void writeAll(const std::string& fileName, const std::string& data);
		static void writeAll(std::ostream& stream, const std::string& data);
	};
}