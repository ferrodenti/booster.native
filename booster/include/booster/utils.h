#pragma once

namespace Booster
{
	template<typename T>
	SP<T> getSingleInstance(std::function<SP<T>()> creator = nullptr)
	{
		static SP<T> instance;
		static std::mutex lock;

		if (!instance)
		{
			std::lock_guard<std::mutex> _(lock);
			if (!instance)
				instance = creator ? creator() : std::make_shared<T>();;
		}
		return instance;
	}

	template<typename T>
	std::shared_ptr<T> makeNotShared(T* obj)
	{
		static auto ignore_delete = [](T* o) {};
		return std::shared_ptr<T>(obj, ignore_delete);
	}

	template<typename T1, typename T2>
	T2 with(T1 obj, std::function<T2(T1 o)> proc)
	{
		if(obj)
			return proc(obj);

		return T2();
	}

	template<typename T1>
	T1 with(T1 obj, std::function<void(T1 o)> proc)
	{
		if (obj)
			proc(obj);

		return obj;
	}

	template<typename T1>
	T1 Or(T1 obj, T1 obj2)
	{
		if (obj)
			return obj;

		return obj2;
	}

	std::string simplifyTypeName(const std::string& type_name);

	std::string formatTimepoint(const std::string& format, const Timepoint& tp);

	std::string base64Encode(unsigned char const* data, unsigned int count);
	std::string base64Encode(const std::vector<u8>& data);
	void base64Decode(std::string const& str, std::vector<u8>& result);

	std::string base36Encode(u64 data);
	u64 base36Decode(std::string const& str);

	template<typename T1>
	std::string typeName()
	{
		static auto res = simplifyTypeName(typeid(T1).name());
		return res;
	}

	/*template<typename T1>
	size_t getHash(const T1& hash)
	{
		static std::hash<T1> hash_fn;
		return hash_fn(hash);
	}*/

	inline u64 getHash(const std::string& value) // FNV-1a algorithm
	{
		auto result = 14695981039346656037ull;
		for (cauto el : value)
		{
			cauto ch = static_cast<u8>(el);
			result = result ^ ch;
			result = result * 1099511628211ull;
		}
		return result;
	}

	inline u64 getHash(const char* data, const size_t count) // FNV-1a algorithm
	{
		auto result = 14695981039346656037ull;
		for (size_t i=0; i<count; ++i)
		{
			cauto ch = static_cast<u8>(data[i]);
			result = result ^ ch;
			result = result * 1099511628211ull;
		}
		return result;
	}

	std::string strerror();

	void randomizeWithTime();
	int rand(int min, int max);
	int rand(int max);
	void randStr(char* str, size_t len);
	std::string randStr(size_t len);

	constexpr u32 makeVer(const u8 major, const u8 minor, const u8 maintenance, const u8 build)
	{
		return (major<<24) + (minor<<16) + (maintenance<<8) + build;
	}


	template <typename T>
	constexpr T rol(T val, int cnt)
	{
		static_assert(std::is_unsigned<T>::value, "Rotate Left only makes sense for unsigned types");
		return (val << cnt) | (val >> (sizeof(T) * 8 - cnt));
	}


	template <typename T>
	constexpr T ror(T val, int cnt)
	{
		static_assert(std::is_unsigned<T>::value, "Rotate right only makes sense for unsigned types");
		return (val >> cnt) | (val << (sizeof(T) * 8 - cnt));
	}
}
