#pragma once

namespace Booster
{
	class __cstr {};
	static __cstr to_c_str;

	class JoinStream : public std::exception
	{
		std::stringstream _stream;
		mutable std::string _temp;
	public:
		JoinStream();
		JoinStream(JoinStream&& stream);

		template<typename T>
		JoinStream operator << (const T& obj)
		{
			write(obj);
			return move(*this);
		}

		const char* operator <<(const __cstr& obj) const;

		template<typename T>
		void write(const T& obj) { _stream << obj; }
		void write(const Timepoint& ts);
		void write(const exception& ex);

		operator std::string() const;
		operator std::wstring() const;
		const char* c_str() const;

		const char* what() const noexcept override;
	};
}
