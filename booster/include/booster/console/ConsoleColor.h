#pragma once

namespace Booster
{
	class ConsoleColor
	{
		int _value;
#ifndef _MSC_VER
		static int _current_fore_color;
		static int _current_back_color;
#endif
	public:
		ConsoleColor();
		explicit ConsoleColor(int value);
		ConsoleColor(const ConsoleColor& proto);
		ConsoleColor(const ConsoleColor& fore_color, const ConsoleColor& back_color);

		ConsoleColor bright() const;
		ConsoleColor dark() const;
		void setCurrent() const;


		bool operator ==(const ConsoleColor& other) const;
		bool operator !=(const ConsoleColor& other) const;

		static void reset();
		static void bold(const bool value);
		static void underline(const bool value);
		static ConsoleColor getCurrent();
		static bool tryParse(const std::string& name, ConsoleColor& result);

		static const ConsoleColor black;
		static const ConsoleColor blue;
		static const ConsoleColor green;
		static const ConsoleColor cyan;
		static const ConsoleColor red;
		static const ConsoleColor magenta;
		static const ConsoleColor yellow;
		static const ConsoleColor light_gray;
		static const ConsoleColor dark_gray;
		static const ConsoleColor light_blue;
		static const ConsoleColor light_green;
		static const ConsoleColor light_cyan;
		static const ConsoleColor light_red;
		static const ConsoleColor light_magenta;
		static const ConsoleColor light_yellow;
		static const ConsoleColor white;
	};

}
