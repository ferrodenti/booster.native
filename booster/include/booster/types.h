#pragma once

#ifdef Booster
#undef Booster
#endif
#define Booster Booster

// ReSharper disable once CppInconsistentNaming
#define cauto const auto

namespace Booster
{
	using i8 = char;
	using i16 = short;
	using i32 = int;
	using i64 = long long;
	//using i128 = __int128;

	using u8 = unsigned char;
	using u16 = unsigned short;
	using u32 = unsigned int;
	using u64 = unsigned long long;

	//using u128 = unsigned __int128;

	using read_lock = std::shared_lock< std::shared_mutex>;
	using write_lock = std::unique_lock< std::shared_mutex>;

	using Timepoint = std::chrono::system_clock::time_point;

	template<typename T>
	using UP = std::unique_ptr<T>;

	template<typename T>
	using SP = std::shared_ptr<T>;

	template<typename T>
	using SCP = std::shared_ptr<const T>;

	template<typename T>
	class Enumerable;

	class JoinStream;
	using log_params = std::initializer_list<std::pair<std::string, std::string>>;
	class LogLevel;
	class Log;
	class LogMessage;
	class LogInput;
	class MessageFormatter;
	//using LogMessagePtr = std::unique_ptr<LogMessage>; //TODO: use unique_ptr
	using LogMessagePtr = std::shared_ptr<LogMessage>;
}
