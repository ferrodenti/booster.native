#if  __APPLE1__
#	include <filesystem>
namespace fs = std::filesystem;
#elif defined(_MSC_VER) | defined(__APPLE__)
#	define _SILENCE_EXPERIMENTAL_FILESYSTEM_DEPRECATION_WARNING
#	include <experimental/filesystem>
namespace fs = std::experimental::filesystem;
#elif defined(__clang__)
#	include <experimental/forward_list>
namespace fs = std::experimental::filesystem;
#endif