#pragma once

namespace Booster
{
	class StringTemplate
	{
		char _symbol = '#';
		std::string _template;

		mutable std::mutex _lock;
		mutable std::vector<std::tuple<std::string,bool>>* _tokens = nullptr;

	public:
		bool cache_values = true;

		StringTemplate();
		explicit StringTemplate(const std::string& tmpl, char symbol = '#');
		~StringTemplate();

		void setup(const std::string& tmpl, char symbol = '#');
		void parse() const;
		std::string format(std::function<std::string(const std::string&)> values_provider) const;
	};
}
