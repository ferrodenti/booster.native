#pragma once

#include "extensions/StringEx.h"

namespace Booster
{
	class EnumBuilder
	{
		std::string _comma;
		std::string _opener;
		std::string _closer;
		std::stringstream _str;
		bool _needComma = false;
		bool _empty = true;
	public:
		EnumBuilder(const std::string& comma = ",", const std::string& opener = "", const std::string& closer = "");

		bool empty() const;
		bool some() const;
		size_t size() const;
		void appendCommaOrOpener();
		void appendOpener();
		void appendNoComma(const std::string& str);
		//TODO: appendForamat;

		std::string toString() const;

		operator StringEx() const;
		operator std::string() const;
		operator const char*() const;

		template<typename T>
		EnumBuilder& operator <<(const T& obj)
		{
			write(obj);
			return *this;
		}

		template<typename T>
		void write(const T& obj)
		{
			appendCommaOrOpener();
			_str << obj;
		}
		void write(const JoinStream& js);
		void write(const Timepoint& ts);
	};
}
