#pragma once

#include "NoCopy.h"

namespace Booster
{
	class ScopeExit : NoCopy
	{
	public:
		explicit ScopeExit(std::function<void()> action);
		~ScopeExit();
	private:
		const std::function<void()> _action;
	};

	
}
