#pragma once

#ifndef BOOSTER_MACROS
#define BOOSTER_MACROS

#define BOOSTER_VS_BUG_FIX(x) x

#define BOOSTER_GET_1ST_ARG(arg1, ...) arg1
#define BOOSTER_GET_2ND_ARG(arg1, arg2, ...) arg2
#define BOOSTER_GET_3RD_ARG(arg1, arg2, arg3, ...) arg3
#define BOOSTER_GET_4TH_ARG(arg1, arg2, arg3, arg4, ...) arg4
#define BOOSTER_GET_5TH_ARG(arg1, arg2, arg3, arg4, arg5, ...) arg5
#define BOOSTER_GET_6TH_ARG(arg1, arg2, arg3, arg4, arg5, arg6, ...) arg6
#define BOOSTER_GET_7TH_ARG(arg1, arg2, arg3, arg4, arg5, arg6, arg7, ...) arg7
#define BOOSTER_GET_8TH_ARG(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, ...) arg8

#endif