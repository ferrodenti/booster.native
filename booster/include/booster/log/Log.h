#pragma once
#include "LogInput.h"
#include "LogMessage.h"
#include <booster/extensions.h>
#include "MessageFormatter.h"
#include <booster/EnumBuilder.h>
#include <booster/utils.h>

#ifndef CATCH_LOG

#ifndef BOOSTER_DEFAULT_LOG_FIELD_NAME
#define BOOSTER_DEFAULT_LOG_FIELD_NAME _log
#endif 

#define CATCH_LOG1(operation) CATCH_LOG3(BOOSTER_DEFAULT_LOG_FIELD_NAME, operation, )
#define CATCH_LOG2(log, operation) CATCH_LOG3(log, operation, )
#define CATCH_LOG3(log, operation, onerror) \
catch (const std::exception& ex) \
{ \
	(log).error << operation << ": " << ex; \
	onerror; \
} \
catch (...) \
{ \
	(log).error << operation << ": unknown error"; \
	onerror; \
}

#define CATCH_LOG_MACRO_CHOOSER(...) BOOSTER_VS_BUG_FIX(BOOSTER_GET_4TH_ARG(__VA_ARGS__, CATCH_LOG3, CATCH_LOG2, CATCH_LOG1, ))
#define CATCH_LOG(...) BOOSTER_VS_BUG_FIX(CATCH_LOG_MACRO_CHOOSER(__VA_ARGS__)(__VA_ARGS__))

#endif

namespace Booster
{
	class Log 
	{
		std::vector<SCP<Log>> _destinations;
		std::unordered_map<std::string, std::string> _parameters;
		mutable std::string _last_date;
	protected:
		explicit Log(const std::string& name);
		virtual bool init(LogMessage& message) const;
		virtual std::string format(LogMessage& message) const;
		virtual void writeDateHeader(const Timepoint& tp) const;
	public:
		Log();
		Log(const Enumerable<SCP<Log>>& destinations, const std::string& name = "");
		Log(const Enumerable<SCP<Log>>& destinations, const std::string& name, const log_params& parameters);
		Log(const Log& proto);

		virtual void addDestination(Log& destination, bool transfer_parameters_of_interest = true);
		virtual void addDestination(SP<Log> destination, bool transfer_parameters_of_interest = true);
		virtual ~Log() = default;
		virtual bool empty() const;

		const LogInput debug;
		const LogInput trace;
		const LogInput warn;
		const LogInput error;
		const LogInput info;
		const LogInput fatal;

		const std::string name;
		bool enabled = true;
		bool date_headers = true;
		MessageFormatter message_formatter;

		LogLevel levels = LogLevel::all;

		void excludeLevel(const LogLevel& level);

		std::function<bool(const LogMessage&)> filter = nullptr;

		virtual void write(LogMessage& message) const;

		static Log noLog();
		Log create(const std::string& log_name, const log_params& parameters) const;
		Log create(const std::string& log_name) const;

		bool catchErrors(std::function<void()> proc, std::function<void(const std::exception& ex)> error = nullptr, std::function<void()> finally_ = nullptr) const;
		bool catchErrors(const std::string& operation, std::function<void()> proc, std::function<void(const std::exception& ex)> error = nullptr, std::function<void()> finally_ = nullptr) const;

		template<typename T>
		Log create() const
		{
			return create(typeName<T>(), {});
		}

		template<typename T>
		Log create(const log_params& parameters) const
		{
			return create(typeName<T>(), parameters);
		}

		template<typename T>
		Log create(const std::string& log_id) const
		{
			return create(EnumBuilder(".") << typeName<T>() << log_id, {});
		}

		template<typename T>
		Log create(const std::string& log_id, const log_params& parameters) const
		{
			return create(EnumBuilder(".") << typeName<T>() << log_id, parameters);
		}
	};
}
