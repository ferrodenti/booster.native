#pragma once

#include "LogLevel.h"

namespace Booster
{
	class LogInput
	{
		const Log& _context;
	public:
		LogInput(const Log& context, LogLevel level);
		LogMessagePtr createMessage() const;
		const LogLevel level;

		template<typename T>
		LogMessagePtr operator << (const T& obj) const
		{
			auto msg = createMessage();
			msg << obj;
			return msg;
		}
	};

}
