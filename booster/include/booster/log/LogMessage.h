#pragma once
#include "LogLevel.h"
#include <booster/NoCopy.h>

namespace Booster
{
	class LogMessage : NoCopy
	{
		const Log* _context = nullptr;
	public:
		LogMessage(const Log* context, LogLevel level);
		~LogMessage();

		LogLevel level;
		Timepoint timestamp;
		bool has_exception = false;
		std::string exception;
		std::unordered_map<std::string, std::string> parameters;
		const MessageFormatter* message_formatter = nullptr;
		std::string formatted;
		std::stringstream buffer;
		std::string ctx_name;

		template<typename T>
		void write(const T& obj)
		{
			buffer << obj;
		}

		void write(const JoinStream& js);
		void write(const Timepoint& ts);
		void write(const std::exception& ex);
		void write(const log_params& param);
	};

	template<typename T>
	LogMessagePtr operator << (LogMessagePtr msg, const T& obj)
	{
		msg->write(obj);
		return msg;
	}
}
