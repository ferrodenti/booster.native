#pragma once
#include "LogMessage.h"

namespace Booster
{
	class MessageFormatter
	{
	public:
		std::string new_line = "\r\n";
		std::string separator = " ";
		std::string param_separator = ",";
		std::string param_opener = ".";
		std::string param_closer = "";
		std::string message_prefix = ":";
		std::string date_format = "== %d-%m-%y ==";
		std::string time_format = "%H:%M:%S";
		int priority = 0;
		bool print_levels = true;

		std::function<std::string(const LogMessage& msg)> custom_format = nullptr;
		std::function<std::string(const LogMessage& msg)> custom_exception_format = nullptr;

		std::vector<std::string> parameters;

		std::string format(const LogMessage& msg) const;
		std::string formatException(const LogMessage& msg) const;
	};
}
