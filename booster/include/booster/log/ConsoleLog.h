#pragma once
#include "Log.h"
#include <booster/console/ConsoleColor.h>

namespace Booster
{
	class ConsoleLog : public Log
	{
		mutable std::mutex _lock;
		void cout(LogMessage& message, const std::string& msg) const;
	public:
		ConsoleLog();
		bool use_colors = true;
		bool restore_color = true;
		std::unordered_map<int, ConsoleColor> colors;

		void write(LogMessage& message) const override;
	};
}
