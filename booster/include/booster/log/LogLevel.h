#pragma once
#include "../enums.h"

namespace Booster
{
	BETTER_ENUM(LogLevel, int,
		debug = 1,
		trace = 2,
		warn = 4,
		error = 8,
		info = 16,
		fatal = 32,
		all = 63
	);
}
