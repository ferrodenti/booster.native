#pragma once
#include "Log.h"

namespace Booster
{
	class FileLog : public Log
	{
		mutable std::mutex _lock;

	public:
		explicit FileLog(std::function<std::string(const LogMessage& msg)> filename);
		explicit FileLog(const std::string& const_file_name);

		std::function<std::string(const LogMessage& msg)> file_name_proc = nullptr;
		std::string filename;
		bool ensure_path = true;

		void write(LogMessage& message) const override;
	};
}
