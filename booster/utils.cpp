#include "stdafx.h"
#include <booster/types.h>
#include <booster/utils.h>
#include <booster/extensions.h>

#if __GNUC__
#include <cstring>
#endif
namespace Booster
{
	std::string simplifyTypeName(const std::string& type_name)
	{
		auto i = $(type_name).lastIndexOf("::");
		if (i >= 0)
			return type_name.substr(i + 2);

		i = $(type_name).indexOf("class");
		if (i >= 0)
			return $($(type_name).substr(i + sizeof "class" - 1)).trim();

		return type_name;
	}

	std::string formatTimepoint(const std::string& format, const Timepoint& tp) //TODO: milliseconds
	{
		auto t = std::chrono::system_clock::to_time_t(tp);
#if __APPLE__
        tm& tm = *localtime(&t);
#else
        tm tm;
		localtime_s(&tm, &t);
#endif

		cauto sz = std::max(64, int(format.size()));
		cauto buff = new char[sz];
		strftime(buff, sz, format.c_str(), &tm);
		std::string res(buff);
		delete[] buff;
		return res;
	}


	static const std::string base64_chars =	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

	static bool isBase64(const unsigned char c) 
	{
		return isalnum(c) || c == '+' || c == '/';
	}

	std::string base64Encode(unsigned char const* data, unsigned int count) 
	{
		std::string ret;
		auto i = 0;
		auto j = 0;
		unsigned char chars3[3], chars4[4];

		while (count--) 
		{
			chars3[i++] = *data++;

			if (i == 3) 
			{
				chars4[0] = (chars3[0] & 0xfc) >> 2;
				chars4[1] = ((chars3[0] & 0x03) << 4) + ((chars3[1] & 0xf0) >> 4);
				chars4[2] = ((chars3[1] & 0x0f) << 2) + ((chars3[2] & 0xc0) >> 6);
				chars4[3] = chars3[2] & 0x3f;

				for (i = 0; i <4; i++)
					ret += base64_chars[chars4[i]];

				i = 0;
			}
		}

		if (i)
		{
			for (j = i; j < 3; j++)
				chars3[j] = '\0';

			chars4[0] = (chars3[0] & 0xfc) >> 2;
			chars4[1] = ((chars3[0] & 0x03) << 4) + ((chars3[1] & 0xf0) >> 4);
			chars4[2] = ((chars3[1] & 0x0f) << 2) + ((chars3[2] & 0xc0) >> 6);
			chars4[3] = chars3[2] & 0x3f;

			for (j = 0; j < i + 1; j++)
				ret += base64_chars[chars4[j]];

			while (i++ < 3)
				ret += '=';

		}
		return ret;
	}

	std::string base64Encode(const std::vector<u8>& data)
	{		
		std::string ret;
		auto i = 0;
		unsigned char chars3[3], chars4[4];

		for(auto it : data)
		{
			chars3[i++] = it;

			if (i == 3)
			{
				chars4[0] = (chars3[0] & 0xfc) >> 2;
				chars4[1] = ((chars3[0] & 0x03) << 4) + ((chars3[1] & 0xf0) >> 4);
				chars4[2] = ((chars3[1] & 0x0f) << 2) + ((chars3[2] & 0xc0) >> 6);
				chars4[3] = chars3[2] & 0x3f;

				for (i = 0; i <4; i++)
					ret += base64_chars[chars4[i]];

				i = 0;
			}
		}

		if (i)
		{
			for (auto j = i; j < 3; j++)
				chars3[j] = '\0';

			chars4[0] = (chars3[0] & 0xfc) >> 2;
			chars4[1] = ((chars3[0] & 0x03) << 4) + ((chars3[1] & 0xf0) >> 4);
			chars4[2] = ((chars3[1] & 0x0f) << 2) + ((chars3[2] & 0xc0) >> 6);
			chars4[3] = chars3[2] & 0x3f;

			for (auto j = 0; j < i + 1; j++)
				ret += base64_chars[chars4[j]];

			while (i++ < 3)
				ret += '=';

		}
		return ret;
	}

	void base64Decode(std::string const& str, std::vector<u8>& result)
	{
		auto size = str.size();
		auto i = 0;
		auto cnt = 0;
		unsigned char chars4[4], chars3[3];

		result.reserve(result.size() + str.size()*2);

		while (size-- && str[cnt] != '=' && isBase64(str[cnt])) 
		{
			chars4[i++] = str[cnt]; cnt++;

			if (i == 4) 
			{
				for (i = 0; i <4; i++)
					chars4[i] = u8(base64_chars.find(chars4[i]));

				chars3[0] = (chars4[0] << 2) + ((chars4[1] & 0x30) >> 4);
				chars3[1] = ((chars4[1] & 0xf) << 4) + ((chars4[2] & 0x3c) >> 2);
				chars3[2] = ((chars4[2] & 0x3) << 6) + chars4[3];

				for (i = 0; i < 3; i++)
					result.push_back(chars3[i]);

				i = 0;
			}
		}

		if (i) 
		{
			for (auto j = i; j <4; j++)
				chars4[j] = 0;

			for (auto j = 0; j <4; j++)
				chars4[j] = u8(base64_chars.find(chars4[j]));

			chars3[0] = (chars4[0] << 2) + ((chars4[1] & 0x30) >> 4);
			chars3[1] = ((chars4[1] & 0xf) << 4) + ((chars4[2] & 0x3c) >> 2);
			chars3[2] = ((chars4[2] & 0x3) << 6) + chars4[3];

			for (auto j = 0; j < i - 1; j++)
				result.push_back(chars3[j]);
		}
	}

	static const std::string base36_chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	std::string base36Encode(u64 data)
	{
		std::stringstream result;

		do
		{
			result << base36_chars[data %36];
			data /= 36;
		}
		while (data);
		return result.str();
	}
		
	u64 base36Decode(std::string const& str)
	{
		u64 result = 0;

		for(auto i= i64(str.size())-1; i >= 0; --i)
		{
			cauto ch = char(toupper(str[i]));

			result *= 36;

			if (ch >= '0' && ch <= '9')
				result += ch - '0';
			else if (ch >= 'A' && ch <= 'Z')
				result += 10 + ch - 'A';
			else
				throw $() << "invalid character: " << ch;
		}
		return result;
	}

	std::string strerror()
	{
		const char* err = std::strerror(errno);
		return $() << err;
	}

	void randomizeWithTime()
	{	
		srand(u32(time(nullptr)));
	}

	int rand(const int min, const int max)
	{
		return min + int(double(std::rand()) / (double(RAND_MAX) + 1) * (max - min));
	}

	int rand(const int max)
	{
		return int(double(std::rand()) / (double(RAND_MAX) + 1) * max);
	}

	void randStr(char* str, const size_t len)
	{
		static const char alphanum[] =
			"0123456789"
			"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
			"abcdefghijklmnopqrstuvwxyz";

		for (size_t i = 0; i < len; ++i)
			str[i] = alphanum[std::rand() % (sizeof alphanum - 1)];

		str[len] = 0;
	}

	std::string randStr(const size_t len)
	{			
		static const char alphanum[] =
			"0123456789"
			"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
			"abcdefghijklmnopqrstuvwxyz";

		std::string res;
		res.reserve(len);

		for (size_t i = 0; i < len; ++i)
			res[i] = alphanum[rand(int(sizeof alphanum))];

		//res[len] = 0;
		return res;
	}

}
