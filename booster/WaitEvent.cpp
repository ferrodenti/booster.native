#include "stdafx.h"
#include <booster/types.h>
#include <booster/WaitEvent.h>

using namespace Booster;

WaitEvent::WaitEvent()
{
	WaitEvent(false);
}

WaitEvent::WaitEvent(bool initial)
	: _flag(initial), _flagAll(initial)
{
}

void WaitEvent::set()
{
	//std::lock_guard<std::mutex> _(_protect);
	_flag = true;
	_signal.notify_one();
}

void WaitEvent::setAll()
{
	//std::lock_guard<std::mutex> _(_protect);
	_flagAll = true;
	_signal.notify_all();
}

void WaitEvent::reset()
{
	//std::lock_guard<std::mutex> _(_protect);
	_flag = _flagAll = false;
}

void WaitEvent::resetAll()
{
	//std::lock_guard<std::mutex> _(_protect);
	_flagAll = false;
}

bool WaitEvent::waitOne(int interval)
{
	std::unique_lock<std::mutex> lk(_protect);

	if(interval == 0) {}
	else if(interval < 0)
	{
		while (!(_flag || _flagAll))
			_signal.wait(lk);
	}
	else if(interval > 0)
		_signal.wait_for(lk, std::chrono::milliseconds(interval));

	bool result = _flag || _flagAll;

	if (_flag)
		_flag = false;

	return result;
}
