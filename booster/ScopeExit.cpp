#include "stdafx.h"
#include <utility>
#include "booster/ScopeExit.h"

Booster::ScopeExit::ScopeExit(std::function<void()> action) 
	: _action(std::move(action))
{
}

Booster::ScopeExit::~ScopeExit()
{
	if (_action) 
		_action();
}