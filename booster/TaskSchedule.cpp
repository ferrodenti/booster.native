#include "stdafx.h"
#include <booster/types.h>
#include <booster/TaskSchedule.h>
#include "include/booster/ScopeExit.h"

using namespace Booster;
using namespace std::chrono;

TaskSchedule::TaskImpl::TaskImpl(TaskSchedule& owner, Timepoint time, u64 period_ms, std::function<void(Task&)> action, bool inclusive) :
	_owner(owner),
	_action(action),
	_inclusive(inclusive),
	time(time),
	period_ms(period_ms)
{
}

void TaskSchedule::TaskImpl::exe(SP<TaskImpl> self)
{
	if(done || canceled)	
		return;
		
	executing = true;
	Task task(self);
	ScopeExit _([=]{ executing = false; });
	if (period_ms > 0)
	{
		if( _inclusive)
		{
			change(self, system_clock::now() + milliseconds(period_ms));
			_action(task);
		}
		else
		{
			_action(task);
			change(self, system_clock::now() + milliseconds(period_ms));
		}
	}
	else
	{			
		not_done = false;
		_action(task);	
		if(!not_done)
			done = true;
	}
}

void TaskSchedule::TaskImpl::cancel(SP<TaskImpl> self)
{
	_owner._queue.remove(self, time);
	canceled = true;
}

void TaskSchedule::TaskImpl::change(SP<TaskImpl> self, Timepoint time)
{
	bool l = _owner._queue.remove(self, time);
	if(executing)
		not_done = true;
	done = canceled = false;
	this->time = time;
	changed ++;
	_owner.plan(self);
}

void TaskSchedule::proc()
{
	std::future<void> future = _exit_signal.get_future();
	ScopeExit _([this]{ _has_thread = false; });
	Timepoint time;
	SP<TaskImpl> task;

	while(future.wait_for(milliseconds(0)) == std::future_status::timeout)
	{
		{
			std::lock_guard<std::mutex> __(_thread_lock);
			if (!_queue.tryPeek(task, time))
				return;
		}

		if(task->time > system_clock::now())
		{	
			/*if(future.wait_until(task->time) != std::future_status::timeout)
				return;*/

			continue;
		}

		std::lock_guard<std::mutex> __(_exe_lock);

		if (_queue.tryDequeue(task, time))
		{
			if (task->time > system_clock::now())
				_queue.tryEnqueue(task, time);
			else
				task->exe(task);
		}
	}
}

TaskSchedule::~TaskSchedule()
{
	_exit_signal.set_value();
	_queue.clear();

	if (_thread.joinable())
		_thread.join();
}

TaskSchedule::Task::Task(SP<TaskImpl> impl) :
	_impl(impl)
{
}

Timepoint TaskSchedule::Task::time() const
{
	return _impl->time;
}

bool TaskSchedule::Task::cancel() const
{
	_impl->cancel(_impl);
	return !_impl->done;
}

bool TaskSchedule::Task::canceled() const
{
	return _impl->canceled;
}

bool TaskSchedule::Task::done() const
{
	return _impl->done;
}

bool TaskSchedule::Task::doneOrCanceled() const
{
	return _impl->done || _impl->canceled;
}

void TaskSchedule::Task::setPeriod(i64 period_ms) const
{
	_impl->period_ms = period_ms;
}

bool TaskSchedule::Task::delay(Timepoint time) const
{
	_impl->change(_impl, time);
	return !_impl->done;
}

bool TaskSchedule::Task::delay(i64 milliseconds) const
{
	return delay(system_clock::now() + microseconds(milliseconds));
}

SP<std::lock_guard<std::mutex>> TaskSchedule::executionLock() const
{	
	return std::make_shared<std::lock_guard<std::mutex>>(_exe_lock);
}

TaskSchedule::Task TaskSchedule::planSingle(i64 milliseconds, std::function<void()> action)
{
	return planSingle(milliseconds, [action](Task&) { action(); });
}

TaskSchedule::Task TaskSchedule::planSingle(Timepoint time, std::function<void()> action)
{		
	return planSingle(time, [action](Task&) { action(); });
}

TaskSchedule::Task TaskSchedule::planRepeating(u64 milliseconds, std::function<void()> action, bool inclusive)
{
	return planRepeating(milliseconds, [action](Task&) { action(); }, inclusive);
}

TaskSchedule::Task TaskSchedule::plan(Timepoint time, u64 period_ms, std::function<void()> action, bool inclusive)
{
	return plan(time, period_ms, [action](Task&) { action();}, inclusive );
}

TaskSchedule::Task TaskSchedule::planSingle(i64 milliseconds, std::function<void(Task&)> action)
{
	return planSingle(system_clock::now() + std::chrono::milliseconds(milliseconds), action);
}

TaskSchedule::Task TaskSchedule::planSingle(Timepoint time, std::function<void(Task&)> action)
{
	return plan(time, 0, action);
}

TaskSchedule::Task TaskSchedule::planRepeating(u64 milliseconds, std::function<void(Task&)> action, bool inclusive)
{
	return plan(system_clock::now() + std::chrono::milliseconds(milliseconds), milliseconds, action, inclusive);
}

TaskSchedule::Task TaskSchedule::plan(Timepoint time, u64 period_ms, std::function<void(Task&)> action, bool inclusive)
{
	auto impl = std::make_shared<TaskImpl>(*this, time, period_ms, action, inclusive);

	plan(impl);

	return Task(impl);
}


void TaskSchedule::plan(SP<TaskImpl> task)
{	
	_queue.tryEnqueue(task, task->time);

	std::lock_guard<std::mutex> _(_thread_lock);

	if (!_has_thread)
	{
		_has_thread = true;

		if (_thread.joinable())
			_thread.join();

		_thread = std::thread(&TaskSchedule::proc, this);
	}
}

