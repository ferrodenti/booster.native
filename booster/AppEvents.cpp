#include "stdafx.h"
#if defined(_WIN32) || defined(_WIN64)
#include <windows.h>
#endif
#include <booster/AppEvents.h>
#include <booster/types.h>
#include <booster/log/Log.h>

using namespace Booster;

Log* AppEvents::log(nullptr);
AppEvents AppEvents::_instance;
Event<AppEvents> AppEvents::quit(&_instance);
Event<AppEvents, const std::exception&> AppEvents::seh_exception(&_instance);

void AppEvents::onQuit()
{
	quit();
}

void AppEvents::onSEHException(const std::exception& ex)
{
	seh_exception(ex);
}

void AppEvents::handleQuitEvent()
{
#if defined(_WIN32) || defined(_WIN64)
	SetConsoleCtrlHandler(PHANDLER_ROUTINE([](auto dwCtrlType)
	{
		if (dwCtrlType == CTRL_CLOSE_EVENT)
		{
			onQuit();
			return 1;
		}
		return 0;
	}), TRUE);
#endif
}

void AppEvents::handleSEHExceptions()
{
#if defined(_WIN32) || defined(_WIN64)
	if (log)
		(*log).debug << "Setting up SEHException handling...";

	_set_se_translator([](auto u, EXCEPTION_POINTERS *pExp)
	{
		std::string error = "SE Exception (";
		switch (u)
		{
		case STILL_ACTIVE: error += "Still_active"; break;
		case EXCEPTION_ACCESS_VIOLATION: error += "Access violation"; break;
		case EXCEPTION_DATATYPE_MISALIGNMENT: error += "Datatype misalignment"; break;
		case EXCEPTION_ARRAY_BOUNDS_EXCEEDED: error += "Array bounds exceeded"; break;
		case EXCEPTION_FLT_DENORMAL_OPERAND: error += "Float denormal_operand"; break;
		case EXCEPTION_FLT_DIVIDE_BY_ZERO: error += "Float divide_by_zero"; break;
		case EXCEPTION_FLT_INEXACT_RESULT: error += "Float inexact_result"; break;
		case EXCEPTION_FLT_INVALID_OPERATION: error += "Float invalid operation"; break;
		case EXCEPTION_FLT_OVERFLOW: error += "Float overflow"; break;
		case EXCEPTION_FLT_STACK_CHECK: error += "Float stack check"; break;
		case EXCEPTION_FLT_UNDERFLOW: error += "Float underflow"; break;
		case EXCEPTION_INT_DIVIDE_BY_ZERO: error += "Integer divide by zero"; break;
		case EXCEPTION_INT_OVERFLOW: error += "Integer overflow"; break;
		case EXCEPTION_PRIV_INSTRUCTION: error += "Priv instruction"; break;
		case EXCEPTION_IN_PAGE_ERROR: error += "In page error"; break;
		case EXCEPTION_ILLEGAL_INSTRUCTION: error += "Illegal instruction"; break;
		case EXCEPTION_NONCONTINUABLE_EXCEPTION: error += "Noncontinuable exception"; break;
		case EXCEPTION_STACK_OVERFLOW: error += "Stack overflow"; break;
		case EXCEPTION_INVALID_DISPOSITION: error += "Invalid disposition"; break;
		case EXCEPTION_GUARD_PAGE: error += "Guard page"; break;
		case EXCEPTION_INVALID_HANDLE: error += "Invalid handle"; break;

		case EXCEPTION_BREAKPOINT: // error += "Breakpoint"; break;
		case EXCEPTION_SINGLE_STEP: //error += "Single step"; break;
		case CONTROL_C_EXIT: //	error += "Control+c exit"; break;
			return;
		default:
			char result[11];
			sprintf_s(result, 11, "0x%08X", u);
			error += result;
		};
		error += ")";
		std::exception ex(error.c_str());
		onSEHException(ex);
		throw ex;
	});


#endif
}

void AppEvents::disableConsoleQuickEdit()
{
#if defined(_WIN32) || defined(_WIN64)
	constexpr i32 quick_edit_mode = 64;
	constexpr i32 extended_flags = 128;
	cauto con_handle = GetStdHandle(STD_INPUT_HANDLE);
	DWORD mode;

	if (!GetConsoleMode(con_handle, &mode))
	{
		std::cout << "GetConsoleMode fail";
		return;
	}
	mode = mode & ~(quick_edit_mode | extended_flags);

	if (!SetConsoleMode(con_handle, mode))
		std::cout << "SetConsoleMode fail";
#endif
}

void AppEvents::setup(const bool disable_error_dialog, const bool handle_seh_exceptions, const bool handle_quit_event)
{
	if(handle_quit_event)
		handleQuitEvent();

	if(handle_seh_exceptions)
		handleSEHExceptions();

#if defined(_WIN32) || defined(_WIN64)
	if(disable_error_dialog)
	{
		constexpr u32 desired = SEM_FAILCRITICALERRORS | SEM_NOALIGNMENTFAULTEXCEPT | SEM_NOGPFAULTERRORBOX | SEM_NOOPENFILEERRORBOX;
		u32 sem = SetErrorMode(desired);
		
		if (log)
		{
			sem = SetErrorMode(desired) - sem;
			(*log).debug << "SetErrorMode=" << std::hex << sem;
		}
	}
#endif
}
