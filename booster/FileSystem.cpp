#include "stdafx.h"
#include <booster/types.h>
#include <booster/FileSystem.h>
#include <booster/utils.h>
#include <booster/extensions.h>
#include <booster/std_filesystem.h>

using namespace Booster;

std::string FileSystem::currentDir()
{
	return fs::current_path().string();
}

bool FileSystem::exists(const std::string& fileName)
{
	fs::path p(fileName);
	return fs::exists(p);
}

bool FileSystem::ensurePath(const std::string& path)
{
	fs::path p(path);
	p.remove_filename();

	if(!fs::exists(p))
	{
		create_directories(p);
		return true;
	}
	return false;
}

bool FileSystem::remove(const std::string& fileName)
{
	fs::path p(fileName);
	if (fs::exists(p))
	{
		fs::remove(p);
		return true;
	}
	return false;
}

std::string FileSystem::ensureDirectoryEnding(const std::string& path)
{
	fs::path p(path);
	p.make_preferred();

	if(p.filename() != ".")
		p += fs::path::preferred_separator;

	return p.generic_string();
}

std::string FileSystem::getFolderName(const std::string& path)
{
	cauto i = static_cast<i64>(path.find_first_of("/\\")); //TODO: учитывать обратные слеши
	if (i < 0)
		return "";

	return path.substr(0, i);
}

std::string FileSystem::getFileName(const std::string& path)
{
	cauto i = static_cast<i64>(path.find_last_of("/\\")); //TODO: учитывать обратные слеши
	if (i < 0)
		return path;

	return path.substr(i + 1);
}

std::fstream FileSystem::openOrCreate(const std::string& fileName, std::ios_base::openmode open_mode)
{
	bool _;
	return openOrCreate(fileName, open_mode, _);
}

std::fstream FileSystem::openOrCreate(const std::string& fileName, std::ios_base::openmode open_mode, bool& created)
{
	std::fstream res;
	created = !openOrCreate(fileName, open_mode, res);
	return res;
}

bool FileSystem::openOrCreate(const std::string& fileName, std::ios_base::openmode open_mode, std::fstream& result)
{
	bool cr = exists(fileName);
	if (!cr)
		open_mode |= std::ios::trunc;

	open(fileName, open_mode, result);
	
	return cr;
}

void FileSystem::open(const std::string& fileName, std::ios_base::openmode open_mode, std::fstream& result)
{
	result.open(fileName, open_mode);

	if (!result.is_open() || !result.good())
		throw $() << "failed to open file " << fileName << ": " << strerror();
}

std::string FileSystem::readAll(const std::string& fileName, bool binary)
{
	std::fstream stream;
	std::ios_base::openmode open_mode = std::ios::in;

	if(binary)
		open_mode |= std::ios::binary;

	open(fileName, open_mode, stream);

	return readAll(stream);
}

std::string FileSystem::readAll(std::istream& stream)
{
	std::stringstream result;
	char buffer[4096];
	while (stream.read(buffer, sizeof buffer))
		result.write(buffer, sizeof buffer);
	result.write(buffer, stream.gcount());
	return result.str();
}

void FileSystem::writeAll(const std::string& fileName, const std::string& data)
{
	std::fstream stream;
	open(fileName, std::ios_base::out | std::ios_base::binary | std::ios_base::trunc, stream);
	writeAll(stream, data);
}

void FileSystem::writeAll(std::ostream& stream, const std::string& data)
{	
	stream.write(data.c_str(), data.size());
}
