#include "stdafx.h"
#include <booster/types.h>
#include <booster/StringTemplate.h>
#include "include/booster/extensions.h"

using namespace Booster;

StringTemplate::StringTemplate()
{
}

StringTemplate::StringTemplate(const std::string& tmpl, char symbol)
{
	setup(tmpl, symbol);
}

StringTemplate::~StringTemplate()
{
	delete _tokens;
}

void StringTemplate::setup(const std::string& tmpl, char symbol)
{
	std::lock_guard<std::mutex> _(_lock);

	_template = tmpl;
	_symbol = symbol;

	if(_tokens)
		delete _tokens;

	_tokens = nullptr;
}

void appendConstToken(std::vector<std::tuple<std::string, bool>>& tokens, const std::string& str)
{
	if (str.empty())
		return;

	auto last = i64(tokens.size()) - 1;
	if (last > 0 && std::get<1>(tokens[last]))
		tokens[last] = make_tuple(std::get<0>(tokens[last]) + str, true);
	else
		tokens.push_back(make_tuple(str, true));
}

void StringTemplate::parse() const
{
	if (_tokens)
		return;

	std::lock_guard<std::mutex> _(_lock);

	if (_tokens)
		return;

	auto tokens = new std::vector<std::tuple<std::string, bool>>();

	auto $tmpl = $(_template);
	size_t pos = 0, sz = $tmpl.size();
	while (pos < sz)
	{
		auto i = $tmpl.indexOf(_symbol, pos);
		if(i < 0)
		{
			appendConstToken(*tokens, $tmpl.substr(pos));
			break;
		}
		auto k = i + 1;
		auto j = $tmpl.indexOf(_symbol, k);
		if( j < 0)
		{
			appendConstToken(*tokens, $tmpl.substr(pos));
			break;
		}

		if( j == k)
			appendConstToken(*tokens, $tmpl.substr(pos, i - pos) + _symbol);
		else
		{
			appendConstToken(*tokens, $tmpl.substr(pos, i - pos));
			tokens->push_back(make_tuple($tmpl.substr(k, j - k), false));
		}
		pos = j + 1;
	}

	_tokens = tokens;
}

std::string StringTemplate::format(std::function<std::string(const std::string&)> values_provider) const
{
	parse();

	std::stringstream res;
	MapEx<std::string, std::string> dict;

	for(auto& pair : *_tokens)
	{
		auto& tok = std::get<0>(pair);
		if(std::get<1>(pair))
			res << tok;
		else if (cache_values)
			res << dict.getOrAdd(tok, [values_provider, tok] { return values_provider(tok); });
		else 
			res << values_provider(tok);
	}

	return res.str();
}
