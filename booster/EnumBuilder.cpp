#include "stdafx.h"
#include <booster/types.h>
#include <booster/EnumBuilder.h>
#include <booster/utils.h>
#include <booster/JoinStream.h>

using namespace Booster;

EnumBuilder::EnumBuilder(const std::string& comma, const std::string& opener, const std::string& closer) :
	_comma(comma),
	_opener(opener),
	_closer(closer)
{
}

bool EnumBuilder::empty() const
{
	return _empty;
}

bool EnumBuilder::some() const
{
	return !_empty;
}

size_t EnumBuilder::size() const
{
	return _empty ? 0 : _str.width() + _closer.size();
}

void EnumBuilder::appendCommaOrOpener()
{
	if(_needComma)
		_str << _comma;
	else
		appendOpener();
}

void EnumBuilder::appendOpener()
{
	if(_empty)
	{
		_str << _opener;
		_empty = false;
	}
	_needComma = true;
}

void EnumBuilder::appendNoComma(const std::string& str)
{
	appendOpener();
	_str << str;
}

std::string EnumBuilder::toString() const
{
	if(_needComma)
		return _str.str() + _closer;

	return _str.str();
}

EnumBuilder::operator StringEx() const
{
	return toString();
}

EnumBuilder::operator std::basic_string<char>() const
{
	return toString();
}

EnumBuilder::operator const char*() const
{
	return toString().c_str();
}

void EnumBuilder::write(const JoinStream& js)
{
	_str << std::string(js);
}

void EnumBuilder::write(const Timepoint& ts)
{
	_str << formatTimepoint("%d-%m-%y %H:%M:%S", ts);
}
