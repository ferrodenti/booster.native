#include "stdafx.h"
#include <booster/types.h>
//#include <booster/utils.h>
#include <booster/BinaryStream.h>
#include <booster/FileSystem.h>
// ReSharper disable once CppUnusedIncludeDirective

using namespace Booster;


BinaryStream::BinaryStream(const std::string& filename, std::ios::openmode open_mode)
{
	cauto stream = std::make_shared<std::fstream>();

	FileSystem::openOrCreate(filename, open_mode, *stream);

	_istream = stream;
	_ostream = stream;
	_istream->seekg(0, std::ios_base::beg);
}

BinaryStream::BinaryStream(const BinaryStream& proto) :
	_istream(proto._istream),
	_ostream(proto._ostream),
	shorten_sizes(proto.shorten_sizes)
{
}

//const BinaryStream& BinaryStream::operator=(const BinaryStream& other) const
//{
//	return BinaryStream(other);
//}

void BinaryStream::writeSize(size_t size)
{							  
	if(shorten_sizes)
	{
		int bytes;
		if(size		  <= 0x1Ful)
			bytes = 0;
		else if (size <= 0x1FFFul)
			bytes = 1;
		else if (size <= 0x1FFFFFul)
			bytes = 2;
		else if (size <= 0x1FFFFFFFul)
			bytes = 3;
		else if (size <= 0x1FFFFFFFFFul)
			bytes = 4;
		else if (size <= 0x1FFFFFFFFFFFul)
			bytes = 5;
		else if (size <= 0x1FFFFFFFFFFFFFul)
			bytes = 6;
		else if (size <= 0x1FFFFFFFFFFFFFFFul)
			bytes = 7;
		else
			throw std::logic_error("size overflow: " + std::to_string(size));

		cauto chars = reinterpret_cast<char*>(&size);
		chars[bytes] |= bytes << 5;

		for(auto i=bytes; i >= 0; --i)
		{
			cauto c = chars[i];
			_ostream->put(c);
		}
	}
	else
		write(size);
}

size_t BinaryStream::readSize()
{
	if (shorten_sizes)
	{
		size_t res = 0;
		cauto chars = reinterpret_cast<char*>(&res);

		const char first = _istream->get();
		cauto bytes = (first >> 5) & 0x7;

		chars[bytes] = first & 0x1F;

		for(int i=bytes-1; i>=0; --i)
			chars[i] = _istream->get();
		
		return res;
	}
	return read<size_t>();
}

std::string BinaryStream::read(identity<std::string>)
{
	const size_t sz = readSize();
	char* buffer = new char[sz];

	_istream->read(buffer, sz);
	std::string res(buffer, sz);

	delete[] buffer;

	return res;
}

std::string BinaryStream::readString(size_t min, size_t max)
{
	const size_t sz = readSize();
	if(sz < min|| sz > max)
		throw std::overflow_error("a size of " + std::to_string(sz) + " bytes does not fall within expected range");

	char* buffer = new char[sz];

	_istream->read(buffer, sz);
	std::string res(buffer, sz);

	delete[] buffer;

	return res;
}

// ReSharper disable CppMemberFunctionMayBeConst
void BinaryStream::writeRaw(const std::string& str)
{
	_ostream->write(str.c_str(), str.size());
}
void BinaryStream::writeRaw(const char* buffer, size_t size)
{
	_ostream->write(buffer, size);
}

void BinaryStream::readRaw(char* buffer, size_t size)
{
	_istream->read(buffer, size);
}

std::string BinaryStream::readRaw(size_t size)
{		
	char* buff = new char[size];
	_istream->read(buff, size);
	std::string res(buff, size);
	delete[] buff;
	return res;
}

std::string BinaryStream::readRest()
{	
	std::stringstream result;
	char buffer[4096];
	while (_istream->read(buffer, sizeof buffer))
		result.write(buffer, sizeof buffer);
	result.write(buffer, _istream->gcount());
	return result.str();
}

void BinaryStream::flush()
{
	_ostream->flush();
}

bool BinaryStream::eof()
{
	return !static_cast<bool>(*_ostream);
}

size_t BinaryStream::tellRead()
{
	return _istream->tellg();
}

size_t BinaryStream::tellWrite()
{
	return _ostream->tellp();
}

void BinaryStream::seekRead(size_t pos)
{
	_istream->seekg(pos);
}

void BinaryStream::seekWrite(size_t pos)
{
	_ostream->seekp(pos);
}

void BinaryStream::seekRead(size_t pos, std::ios_base::seekdir origin)
{
	_istream->seekg(pos, origin);
}

void BinaryStream::seekWrite(size_t pos, std::ios_base::seekdir origin)
{
	_ostream->seekp(pos, origin);
}

// ReSharper restore CppMemberFunctionMayBeConst


