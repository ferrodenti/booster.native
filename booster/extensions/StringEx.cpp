#include "stdafx.h"
#include <booster/types.h>
#include <booster/extensions/StringEx.h>
#include <booster/EnumBuilder.h>

using namespace Booster;

bool StringEx::some(const std::string& str)
{
	return !str.empty();
}

std::string StringEx::trim(const std::string& str, const std::string& chars)
{
	return trimEnd(trimStart(str, chars), chars);
}

std::string StringEx::trimStart(const std::string& str, const std::string& chars)
{
	for(size_t i=0, len = str.size(); i<len; ++i)
	{
		char c = str[i];

		if(chars.find(c) == npos)
			return str.substr(i);
	}
	return "";
}

std::string StringEx::trimEnd(const std::string& str, const std::string& chars)
{
	for (i64 i = str.size() - 1; i >= 0; --i)
	{
		char c = str[i];

		if (chars.find(c) == npos)
			return str.substr(0, i + 1);
	}
	return "";
}

std::string StringEx::replace(const std::string& str, const std::string& from, const std::string& with)
{	
	size_t pos = 0;
	std::string s(str);

	while ((pos = s.find(from, pos)) != npos)
	{	
		s.replace(pos, from.length(), with);
		pos += with.length();
	}

	return s;
}

Enumerable<StringEx> StringEx::split(std::string str, const std::string& separator, bool remove_empty)
{
	size_t ssize = separator.size();

	return Enumerable<StringEx>::yield<size_t>([=](auto& state)
	{
		do
		{
			i64 i = str.find(separator, state.position);
			if (i < 0)
			{
				state.value = str.substr(state.position);
				state.done = true;
				return;
			}
			auto len = i - state.position;
			if(len == 0 && remove_empty)
			{
				state.position = i + ssize;
				continue;
			}
			
			state.value = str.substr(state.position, len);
			state.position = i + ssize;
			return;
		}
		while (true);
	});
}

Enumerable<StringEx> StringEx::split(std::string str, char separator, bool remove_empty)
{
	return Enumerable<StringEx>::yield<size_t>([=](auto& state)
	{
		do
		{
			i64 i = str.find(separator, state.position);
			if (i < 0)
			{
				state.value = str.substr(state.position);
				state.done = true;
				return;
			}
			auto len = i - state.position;
			if (len == 0 && remove_empty)
			{
				state.position = i + 1;
				continue;
			}

			state.value = str.substr(state.position, len);
			state.position = i + 1;
			return;
		} while (true);
	});
}

std::string StringEx::toLower(std::string str)
{
	auto beg = str.begin();
	transform(beg, str.end(), beg, tolower);
	return str;
}

std::string StringEx::toUpper(std::string str)
{
	auto beg = str.begin();
	transform(beg, str.end(), beg, toupper);
	return str;
}

bool StringEx::equals(const std::string& a, const std::string& b, bool no_case)
{
	if(no_case)
		return equalsNoCase(a, b);

	return a == b;
}

bool StringEx::equalsNoCase(const std::string& a, const std::string& b)
{
	if (a.size() != b.size())
		return false;

	return toLower(a) == toLower(b);
}

//#include <cstdarg>
//StringEx StringEx::format(...) const
//{
//	i64 final_n, n = i64(size()) * 2; /* Reserve two times as much as the length of the fmt_str */
//	std::unique_ptr<char[]> formatted;
//	va_list ap;
//	while (1) {
//		formatted.reset(new char[n]); /* Wrap the plain char array into the unique_ptr */
//		strcpy_s(&formatted[0], n, c_str());
//		va_start(ap, c_str());
//		final_n = vsnprintf(&formatted[0], n, c_str(), ap);
//		va_end(ap);
//		if (final_n < 0 || final_n >= n)
//			n += abs(final_n - n + 1);
//		else
//			break;
//	}
//	return std::string(formatted.get());
//}

StringEx StringEx::join(const Enumerable<std::string>& ie) const
{
	EnumBuilder str(*this);

	for(auto& s : ie)
		str << s;

	return str;
}

std::wstring StringEx::toWString() const
{
	return std::wstring(begin(), end());
}

bool StringEx::operator==(const char* c) const
{
	return *this == StringEx(c);
}
