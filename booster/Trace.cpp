#include "stdafx.h"
#include <booster/types.h>
#include <booster/Trace.h>
#include <booster/utils.h>

#ifdef _MSC_VER
#include <windows.h>
#endif

using namespace Booster;

Trace::~Trace()
{
#ifdef _MSC_VER
	std::basic_ostringstream<TCHAR> s;
	s << _stream.str().c_str();
	OutputDebugString(LPCWSTR(s.str().c_str()));
#else
	std::clog << _stream.str();
	// or std::cerr << (x) << std::flush
#endif

}

void Trace::write(const Timepoint& ts)
{
	_stream << formatTimepoint("%d-%m-%y %H:%M:%S", ts);
}

void Trace::write(const std::exception& ex)
{
	_stream << ex.what();
}
