#include "stdafx.h"
#include <booster/types.h>
#include <booster/JoinStream.h>
#include <booster/utils.h>
#include <booster/extensions.h>

using namespace Booster;

JoinStream::JoinStream()
{
}

JoinStream::JoinStream(JoinStream&& stream) :
	_stream(move(stream._stream))
{
}

const char* JoinStream::operator<<(const __cstr& obj) const
{
	return c_str();
}

void JoinStream::write(const Timepoint& ts)
{
	_stream << formatTimepoint("%d-%m-%y %H:%M:%S", ts);
}

void JoinStream::write(const exception& ex)
{
	_stream << ex.what();
}

JoinStream::operator std::string() const
{
	return _stream.str(); 
}

JoinStream::operator std::wstring() const
{
	return $(_stream.str()).toWString();
}

const char* JoinStream::c_str() const
{
	_temp = _stream.str();
	return _temp.c_str();
}

const char* JoinStream::what() const noexcept
{
	return c_str();
}
