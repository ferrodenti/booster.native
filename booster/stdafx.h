// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

//#define _ITERATOR_DEBUG_LEVEL 0

#ifdef _MSC_VER
#	define _CRT_SECURE_NO_WARNINGS
#	define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#	include <SDKDDKVer.h>
#endif

#include <shared_mutex>
#include <string>
#include <vector>
#include <algorithm>
#include <functional>
#include <unordered_map>
#include <map>
#include <mutex>

#include <atomic>
#include <mutex>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <iostream>
