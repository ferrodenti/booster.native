#include "stdafx.h"
#include <booster/types.h>
#include <booster/utils.h>
#include <booster/log/Log.h>
#include <booster/ScopeExit.h>

using namespace Booster;

Log::Log(const std::string& name) :
	debug(*this, LogLevel::debug),
	trace(*this, LogLevel::trace),
	warn(*this, LogLevel::warn),
	error(*this, LogLevel::error),
	info(*this, LogLevel::info),
	fatal(*this, LogLevel::fatal),
	name(name)
{
}

Log::Log(const Enumerable<SCP<Log>>& destinations, const std::string& name, const log_params& parameters) :
	Log(name)
{
	$(_destinations).addRange(destinations);
	$(_parameters).addRange(parameters);
}

Log::Log(const Enumerable<SCP<Log>>& destinations, const std::string& name) :
	Log(destinations, name, {})
{
}

Log::Log() : Log("")
{
}

Log::Log(const Log& proto) :
	Log(proto.name)
{
	_destinations = proto._destinations;
	_parameters = proto._parameters;
}

void Log::addDestination(Log& destination, bool transfer_parameters_of_interest)
{
	addDestination(makeNotShared(&destination), transfer_parameters_of_interest);
}

void Log::addDestination(SP<Log> destination, bool transfer_parameters_of_interest)
{
	_destinations.push_back(destination);

	if (transfer_parameters_of_interest)
		$(destination->message_formatter.parameters).addRange(message_formatter.parameters);
}

bool Log::empty() const
{
	return _destinations.empty();
}

bool Log::init(LogMessage& message) const
{
	if( enabled && levels & message.level && (!filter || !filter(message)))
	{
		for(auto& pair : _parameters)
			if(!$(message.parameters).containsKey(pair.first))
				message.parameters[pair.first] = pair.second;

		if(message_formatter.priority > 0 && (!message.message_formatter || message.message_formatter->priority < message_formatter.priority))
			message.message_formatter = &message_formatter;

		return true;
	}
	return false;
}

std::string Log::format(LogMessage& message) const //TODO: ��������� ������
{
	if(message.message_formatter && message.message_formatter->priority > message_formatter.priority)
	{
		if (message.formatted.empty())
			message.formatted = message.message_formatter->format(message);

		return message.formatted;
	}
	return message_formatter.format(message);
}

void Log::excludeLevel(const LogLevel& level)
{
	levels = LogLevel::_from_integral_unchecked(levels - level);
}

void Log::writeDateHeader(const Timepoint& tp) const
{
	if(date_headers)
	{
		auto sdt = formatTimepoint(message_formatter.date_format, tp);
		if (sdt != _last_date)
		{
			_last_date = sdt;
			MessageFormatter fmt;
			fmt.priority = std::numeric_limits<int>::max();

			LogMessage msg(this, LogLevel::all);
			msg.message_formatter = &fmt;
			// ReSharper disable once CppAssignedValueIsNeverUsed
			msg.formatted = sdt;
		}
	}
}

void Log::write(LogMessage& message) const
{
	if(init(message))
	{
		if(_destinations.size() == 1)
			_destinations[0]->write(message);
		else
		{
			auto param = message.parameters;
			auto fmt = message.message_formatter;
			auto text = message.formatted;

			for (auto dst : _destinations)
			{
				dst->write(message);
				message.parameters = param;
				message.message_formatter = fmt;
				message.formatted = text;
			}
		}
	}
}

Log Log::noLog()
{
	return Log("empty");
}


Log Log::create(const std::string& log_name, const log_params& parameters) const
{
#ifdef _MSC_VER
    if(!this)
		return Log({}, log_name, parameters);
#endif

	return Log({ makeNotShared(this) }, log_name, parameters);
}

Log Log::create(const std::string& log_name) const
{
	return create(log_name, {});
}

bool Log::catchErrors(std::function<void()> proc, std::function<void(const std::exception& ex)> error, std::function<void()> finally_) const
{
	return catchErrors("", proc, error, finally_);
}

bool Log::catchErrors(const std::string& operation, std::function<void()> proc, std::function<void(const std::exception& ex)> error, std::function<void()> finally_) const
{
	ScopeExit _(finally_);

	try
	{
		proc();
		return false;
	}
	catch(const std::exception& ex)
	{
		if(operation.empty())
			this->error << ex;
		else
			this->error << operation << ": " << ex;

		if (error)
			error(ex);
	}
	catch(...)
	{
		std::exception_ptr a = std::current_exception();

		if (operation.empty())
			this->error << "unknown error";
		else
			this->error << operation << ": unknown error";

		if (error)
			error(std::logic_error("unknown error"));
	}
	return true;
}
