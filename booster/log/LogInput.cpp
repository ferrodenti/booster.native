#include "stdafx.h"
#include <booster/types.h>
#include <booster/log/LogInput.h>
#include <booster/log/LogMessage.h>

using namespace Booster;

LogInput::LogInput(const Log& context, LogLevel level) :
	_context(context),
	level(level)
{
}

LogMessagePtr LogInput::createMessage() const
{
	//return std::make_unique<LogMessage>(&_context, level);
	return std::make_shared<LogMessage>(&_context, level);
}


