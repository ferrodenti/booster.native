#include "stdafx.h"
#include <booster/types.h>
#include <booster/log/LogMessage.h>
#include <booster/log/Log.h>
#include <booster/utils.h>

using namespace Booster;

LogMessage::LogMessage(const Log* context, const LogLevel level) :
	_context(context),
	level(level)
{
	timestamp = std::chrono::system_clock::now();

	if(_context)
		ctx_name = _context->name;
}

LogMessage::~LogMessage()
{
	try
	{
		if (_context)
			_context->write(*this);
	}
	catch(...) {}
}

void LogMessage::write(const JoinStream& js)
{
	buffer << std::string(js);
}

void LogMessage::write(const Timepoint& ts)
{
	buffer << formatTimepoint("%d-%m-%y %H:%M:%S", ts);
}

void LogMessage::write(const std::exception& ex)
{
	exception = ex.what();
	has_exception = true;
}

void LogMessage::write(const log_params& param)
{
	parameters = UnorderedMapEx<std::string, std::string>(param);
}
