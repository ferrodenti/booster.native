#include "stdafx.h"
#include <booster/types.h>
#include <booster/log/MessageFormatter.h>
#include <booster/EnumBuilder.h>
#include <booster/extensions.h>
#include <booster/utils.h>

using namespace Booster;

std::string MessageFormatter::format(const LogMessage& msg) const
{
	if(custom_format)
		return custom_format(msg);

	EnumBuilder str(separator);

	if(print_levels)
	{
		str.appendNoComma(msg.level._to_string());
		str.appendNoComma("\t");
	}

	if(!time_format.empty())
		str.appendNoComma(formatTimepoint(time_format, msg.timestamp));

	bool before_msg;
	if ((before_msg = !msg.ctx_name.empty()))
		str << msg.ctx_name;

	if(msg.parameters.size())
	{
		EnumBuilder param(param_separator, param_opener, param_closer);
		std::string s;
		for(auto& p : parameters)
		{
			if(p[0] == '$')
			{
				auto pn = p.substr(1);
				if( $(msg.parameters).tryGetValue(pn, s))
					param << pn + ": " + s;
			}
			else if ($(msg.parameters).tryGetValue(p, s))
				param << s;
		}
		if(!str.empty())
		{
			str.appendNoComma(param);
			before_msg = true;
		}
	}

	if (!msg.buffer.width())
	{
		if(before_msg)
			str.appendNoComma(message_prefix);

		str << msg.buffer.str();
	}
	if(msg.has_exception)
		str << formatException(msg);

	return str;
}

std::string MessageFormatter::formatException(const LogMessage& msg) const
{
	if (custom_exception_format)
		return custom_exception_format(msg);

	return new_line + "Exception: " + msg.exception;
}
