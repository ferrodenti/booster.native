#include "stdafx.h"
#include <booster/types.h>
#include <booster/log/FileLog.h>
#include <booster/FileSystem.h>

using namespace Booster;

FileLog::FileLog(const std::string& const_file_name):
	Log("FileLog"),
	filename(const_file_name)
{
	message_formatter.new_line = "\n";
}

FileLog::FileLog(std::function<std::string(const LogMessage& msg)> filename):
	Log("FileLog"),
	file_name_proc(filename)
{
	message_formatter.new_line = "\n";
}

void FileLog::write(LogMessage& message) const
{
	if(init(message))
	{
		writeDateHeader(message.timestamp);

		std::string fn = file_name_proc ? file_name_proc(message) : filename;
		std::string msg = format(message);
		std::lock_guard<std::mutex> _(_lock);

		if(ensure_path)
			FileSystem::ensurePath(fn);

		std::ofstream file(fn, std::ofstream::out | std::ofstream::app);
		file << msg << message_formatter.new_line;
	}
}
