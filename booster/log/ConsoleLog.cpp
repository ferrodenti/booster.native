#include "stdafx.h"
#include <booster/types.h>
#include <booster/log/ConsoleLog.h>
#include <booster/log/LogLevel.h>
#include "booster/extensions.h"

using namespace Booster;

ConsoleLog::ConsoleLog() :
	Log("ConsoleLog")
{
	message_formatter.print_levels = false;

	colors[LogLevel::debug] = ConsoleColor::dark_gray;
	colors[LogLevel::trace] = ConsoleColor::light_gray;
	colors[LogLevel::warn] = ConsoleColor::light_yellow;
	colors[LogLevel::error] = ConsoleColor::light_red;
	colors[LogLevel::info] = ConsoleColor::light_green;
	colors[LogLevel::fatal] = ConsoleColor(ConsoleColor::dark_gray, ConsoleColor::white);
}

void ConsoleLog::write(LogMessage& message) const
{
	if(init(message))
	{
		writeDateHeader(message.timestamp);

		auto msg = format(message);
		if(!use_colors)
		{
			cout(message, msg);
			return;
		}
			
		ConsoleColor color;
		std::string str;
		if(!$(message.parameters).tryGetValue("color", str) || !ConsoleColor::tryParse(str, color) )
			color = $(colors).get(message.level, ConsoleColor::white);

		std::lock_guard<std::mutex> _(_lock);

		if (restore_color)
		{
			auto prev = ConsoleColor::getCurrent();
			color.setCurrent();

			cout(message, msg);
			ConsoleColor::reset();
			prev.setCurrent();
		}
		else
		{
			color.setCurrent();
			cout(message, msg);
		}
	}
}

void ConsoleLog::cout(LogMessage& message, const std::string& msg) const
{
	//if (message.level == +LogLevel::error || message.level == +LogLevel::fatal)
	//	std::cerr << msg << message_formatter.new_line;
	//else
	std::cout << msg << message_formatter.new_line;
}
