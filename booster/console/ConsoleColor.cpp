#include "stdafx.h"
#include <booster/types.h>
#include <booster/console/ConsoleColor.h>
#include <booster/extensions.h>
#ifdef _MSC_VER
#include <windows.h>
#else
#include <iostream>
#endif

using namespace Booster;
#ifdef _MSC_VER
static HANDLE consoleHandle()
{
	static HANDLE _consoleHandle = nullptr;

	if (!_consoleHandle)
		_consoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);

	return _consoleHandle;
}
const ConsoleColor ConsoleColor::black = ConsoleColor(0);
const ConsoleColor ConsoleColor::blue = ConsoleColor(1);
const ConsoleColor ConsoleColor::green = ConsoleColor(2);
const ConsoleColor ConsoleColor::cyan = ConsoleColor(3);
const ConsoleColor ConsoleColor::red = ConsoleColor(4); //-V112
const ConsoleColor ConsoleColor::magenta = ConsoleColor(5);
const ConsoleColor ConsoleColor::yellow = ConsoleColor(6);
const ConsoleColor ConsoleColor::light_gray = ConsoleColor(7);
const ConsoleColor ConsoleColor::dark_gray = ConsoleColor(8);
const ConsoleColor ConsoleColor::light_blue = ConsoleColor(9);
const ConsoleColor ConsoleColor::light_green = ConsoleColor(10);
const ConsoleColor ConsoleColor::light_cyan = ConsoleColor(11);
const ConsoleColor ConsoleColor::light_red = ConsoleColor(12);
const ConsoleColor ConsoleColor::light_magenta = ConsoleColor(13);
const ConsoleColor ConsoleColor::light_yellow = ConsoleColor(14);
const ConsoleColor ConsoleColor::white = ConsoleColor(15);
#else
const ConsoleColor ConsoleColor::black = ConsoleColor(30);
const ConsoleColor ConsoleColor::red = ConsoleColor(31);
const ConsoleColor ConsoleColor::green = ConsoleColor(32);
const ConsoleColor ConsoleColor::yellow = ConsoleColor(33);
const ConsoleColor ConsoleColor::blue = ConsoleColor(34);
const ConsoleColor ConsoleColor::magenta = ConsoleColor(35);
const ConsoleColor ConsoleColor::cyan = ConsoleColor(36);
const ConsoleColor ConsoleColor::light_grey = ConsoleColor(37);
const ConsoleColor ConsoleColor::dark_grey = ConsoleColor(90);
const ConsoleColor ConsoleColor::light_red = ConsoleColor(91);
const ConsoleColor ConsoleColor::light_green = ConsoleColor(92);
const ConsoleColor ConsoleColor::light_yellow = ConsoleColor(93);
const ConsoleColor ConsoleColor::light_blue = ConsoleColor(94);
const ConsoleColor ConsoleColor::light_magenta = ConsoleColor(95);
const ConsoleColor ConsoleColor::light_cyan = ConsoleColor(96);
const ConsoleColor ConsoleColor::white = ConsoleColor(97);

int ConsoleColor::_current_fore_color = 37;
int ConsoleColor::_current_back_color = 0;
#endif
static std::mutex _lock;


const std::unordered_map<std::string, ConsoleColor> _byNames = {
	{ "black" , ConsoleColor::black },
	{ "blue" , ConsoleColor::blue },
	{ "green" , ConsoleColor::green },
	{ "cyan" , ConsoleColor::cyan },
	{ "red" , ConsoleColor::red },
	{ "magenta" , ConsoleColor::magenta },
	{ "yellow" , ConsoleColor::yellow },
	{ "light_grey" , ConsoleColor::light_gray },
	{ "dark_grey" , ConsoleColor::dark_gray },
	{ "light_blue" , ConsoleColor::light_blue },
	{ "light_green" , ConsoleColor::light_green },
	{ "light_cyan" , ConsoleColor::light_cyan },
	{ "light_red" , ConsoleColor::light_red },
	{ "light_magenta" , ConsoleColor::light_magenta },
	{ "light_yellow" , ConsoleColor::light_yellow },
	{ "white" , ConsoleColor::white }
};


ConsoleColor::ConsoleColor() : 
	_value(0)
{
}

ConsoleColor::ConsoleColor(int value) : 
	_value(value)
{
}

ConsoleColor::ConsoleColor(const ConsoleColor& proto) = default;

ConsoleColor::ConsoleColor(const ConsoleColor& fore_color, const ConsoleColor& back_color) :
	_value(fore_color._value + 16 * back_color._value)
{
}

ConsoleColor ConsoleColor::bright() const
{
	return ConsoleColor(_value + 8);
}

ConsoleColor ConsoleColor::dark() const
{
	return ConsoleColor(_value - 8);
}

ConsoleColor ConsoleColor::getCurrent()
{
#ifdef _MSC_VER
	CONSOLE_SCREEN_BUFFER_INFO info;
	if (!GetConsoleScreenBufferInfo(consoleHandle(), &info))
		return ConsoleColor(0);

	return ConsoleColor(info.wAttributes);
#else
	return ConsoleColor(_current_fore_color);
#endif
}

bool ConsoleColor::tryParse(const std::string& name, ConsoleColor & result)
{
	cauto nm = $(name).toLower().trim();

	ConsoleColor* res;
	if ($(_byNames).tryGetValue(nm, res))
	{
		result = *res;
		return true;
	}

	try
	{
		result = ConsoleColor(stoi(nm));
		return true;
	}
	catch (...) {}
	return false;
}

void ConsoleColor::setCurrent() const
{
	std::lock_guard<std::mutex> _(_lock);
#ifdef _MSC_VER
	FlushConsoleInputBuffer(consoleHandle());
	SetConsoleTextAttribute(consoleHandle(), _value);
#else
	_current_fore_color = _value;
	std::cout << "\033[" << _value << "m";
#endif
}

bool ConsoleColor::operator==(const ConsoleColor& other) const
{
	return _value == other._value;
}

bool ConsoleColor::operator!=(const ConsoleColor& other) const
{
	return _value != other._value;
}

void ConsoleColor::bold(const bool value)
{
#ifndef _MSC_VER
	std::lock_guard<std::mutex> _(_lock);
	std::cout << "\033[" << (value ? 1 : 21) << "m";
#endif
}

void ConsoleColor::underline(const bool value)
{
#ifndef _MSC_VER
	std::lock_guard<std::mutex> _(_lock);
	std::cout << "\033[" << (value ? 4 : 24) << "m";
#endif
}

void ConsoleColor::reset()
{
#ifndef _MSC_VER
	std::lock_guard<std::mutex> _(_lock);
	_current_fore_color = 0;
	_current_back_color = 0;
	std::cout << "\033[1m";
#endif
}
