#include "stdafx.h"
#include <filesystem>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
namespace fs = std::experimental::filesystem;

namespace unit_tests
{		
	TEST_CLASS(IndexedDirectoryTests)
	{
	public:

		TEST_METHOD(IndexedDirectoryTest)
		{
			std::string dir = "test_dir";
			std::string path = dir + "/.test";

			SP<IndexedDirectory> id = IndexedDirectory::get(path);

			test(id);

			id->dispose();

			test(id);

			IndexedDirectory::remove(id);
			id.reset();

			system(("rmdir /s /q " + dir).c_str());
			//fs::remove_all(path);
		}

		TEST_METHOD(IndexedDirectoryExtTest)
		{
			std::string dir = "test_dir";
			std::string path = dir + "/.test";

			SP<IndexedDirectory> id = IndexedDirectory::get(path);

			test(id);

			IndexedDirectory::remove(id);

			id = IndexedDirectory::get(path);

			test(id);

			IndexedDirectory::remove(id);
			id.reset();

			system(("rmdir /s /q " + dir).c_str());
			//fs::remove_all(path);
		}

		static void test(SP<IndexedDirectory> id)
		{
			std::vector<std::thread> threads;
			for (int i = 0; i<10; i++)
			{
				threads.emplace_back([=]()
				{
					try
					{
						for (int j = 0; j<100; j++)
						{
							auto fn = id->getNewFilename(false);

							if (FileSystem::exists(fn))
								Assert::Fail();

							std::fstream stream;
							FileSystem::openOrCreate(fn, std::fstream::binary | std::fstream::in | std::fstream::out, stream);

							stream << j;
						}
					}
					catch(const std::exception& ex)
					{
						ex.what();
					}
					
				});
			}

			for (auto& thread : threads)
				thread.join();
		}
	};
}