#include "stdafx.h"
#include <random>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace unit_tests
{		
	TEST_CLASS(BinaryStreamTests)
	{
	public:
		TEST_METHOD(sizeReadWriteTest)
		{
			auto seed = std::chrono::system_clock::now().time_since_epoch().count();
			std::mt19937_64 rnd64(seed);

			for(int i=0; i<100000; ++i)
			{
				std::stringstream str;
				auto num_bytes = 1ul + rnd64() % 8ul;
				auto mask = ~(0xFFFFFFFFFFFFFFFFul << num_bytes*8) & 0x1FFFFFFFFFFFFFFFul;
				size_t expected = rnd64() & mask;

				BinaryStream stream(str);
				//stream.shorten_sizes = false;

				stream.writeSize(expected);
				size_t actual = stream.readSize();

				Assert::AreEqual(expected, actual);
			}
		}
	};
}