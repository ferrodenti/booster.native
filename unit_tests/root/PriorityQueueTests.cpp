#include "stdafx.h"
#include <ctime>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace unit_tests
{		
	TEST_CLASS(PriorityQueueTests)
	{
	public:
		TEST_METHOD(priorityQueueTest)
		{
			srand(u32(time(nullptr)));

			for (int i = 0; i < 10000; i++)
			{
				PriorityQueue<int> q;
				std::vector<float> data;

				for (int j = 0; j < 100; j++)
				{
					float f = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
					q.tryEnqueue(j + 1, f);
					data.push_back(f);
				}

				int k;
				float last = 0, p;
				while (q.tryDequeue(k, p))
				{
					Assert::IsTrue(last <= p);
					last = p;
				}
			}
		}
	};
}