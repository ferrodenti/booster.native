#include "stdafx.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace unit_tests
{		
	TEST_CLASS(BaseEncondingsTests)
	{
	public:
		TEST_METHOD(Base36Test)
		{
			srand(u32(time(nullptr)));
			for(int i=0; i<1000; i++)
			{
				u64 r = (u64(rand()) << 32) + rand();
				auto str = base36Encode(r);
				u64 a = base36Decode(str);
				Assert::AreEqual(a, r);
			}
		}
	};
}