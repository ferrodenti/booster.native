#include "stdafx.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace unit_tests
{		
	TEST_CLASS(StringTemplateTests)
	{
	public:
		TEST_METHOD(StringTemplateSimpleTest)
		{
			StringTemplate st("This #is###a #test# template #qwe");
			Assert::AreEqual(std::string("This IS#a TEST template #qwe"), st.format([](auto &t) { return $(t).toUpper(); }));
		}

		std::string randomAsciiString(size_t length) const
		{
			auto randchar = []() -> char
			{
				const char charset[] =
					"0123456789"
					"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
					"abcdefghijklmnopqrstuvwxyz";
				const size_t max_index = sizeof charset - 1;
				return charset[rand() % max_index];
			};
			std::string str(length, 0);
			std::generate_n(str.begin(), length, randchar);
			return str;
		}

		TEST_METHOD(StringTemplateRandomTest)
		{
			srand(u32(time(nullptr)));

			for(int i=0; i<1000; i++)
			{
				MapEx<std::string, std::string> tokens;
				std::stringstream tmpl;
				std::stringstream expect;

				int len = rand() % 10;

				for(int j=0; j<len; j++)
				{
					auto s = randomAsciiString(1 + rand() % 10);
					switch(rand() % 3)
					{
					case 0:
						tmpl << s;
						expect << s;
						break;
					case 1:
					{
						while (tokens.containsKey(s))
							s = randomAsciiString(1 + rand() % 10);

						auto v = randomAsciiString(1 + rand() % 10);

						tokens[s] = v;
						tmpl << '#' << s << '#';
						expect << v;
						break;
					}
					case 2:
						tmpl << "##";
						expect << "#";
						break;
					}
				}

				if (rand() % 2)
				{
					auto s = "#" + randomAsciiString(1 + rand() % 10);
					tmpl << s;
					expect << s;
				}

				StringTemplate st(tmpl.str());
				Assert::AreEqual(expect.str(), st.format([&tokens](auto &t) { return tokens[t]; }));
			}
		}
	};
}