#include "stdafx.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace unit_tests
{		
	TEST_CLASS(TaskScheduleTests)
	{
	public:
		i64 passedMs(Timepoint& from)
		{
			return duration_cast<milliseconds>(system_clock::now() - from).count();
		}

		TEST_METHOD(TaskScheduleTest)
		{
			TaskSchedule schedule;
			int cnt = 5;
			bool neg = false;
			bool neg2 = false;
			std::promise<void> signal;
			std::future<void> future = signal.get_future();
			Timepoint start = std::chrono::system_clock::now();

			auto task1 = schedule.planSingle(50, [&](auto& t)
			{			
				TRACE << "task1: " << passedMs(start) << " cnt=" << cnt << "\n";
				if(cnt > 0)
				{	
					cnt --;
					t.delay(30);
				}
				else
					signal.set_value();
			});

			auto task2 = schedule.planSingle(310, [&](auto& t)
			{
				TRACE << "task2: " << passedMs(start) << "\n";
				neg = true;
			});
			auto task3 = schedule.planSingle(200, [&](auto& t)
			{
				TRACE << "task3: " << passedMs(start) << "\n";
				neg2 = true;
			});
			auto task4 = schedule.planRepeating(10, [&](auto& t)
			{
				TRACE << "task4: " << passedMs(start) << "\n";
				task3.delay(200);
				t.delay(10);
			});

			future.wait();
			task1.cancel();
			task2.cancel();
			task3.cancel();
			task4.cancel();
			TRACE << "test over in " << passedMs(start) << "ms\n";
			std::this_thread::sleep_for(milliseconds(6000));

			Assert::AreEqual(0, cnt);
			//Assert::IsFalse(neg);
			//Assert::IsFalse(neg2);
		}
	};
}