#include "stdafx.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace unit_tests
{		
	TEST_CLASS(JoinStreamTests)
	{
	public:
		template<typename T>
		T impl(T o)
		{
			return o;
		}

		TEST_METHOD(joinTest)
		{
			Assert::AreEqual(std::string("qwe123"), impl<std::string>($() << "qwe" << 1 << 2 << 3));
			Assert::AreEqual($("qwe123").toWString(), impl<std::wstring>($() << "qwe" << 1 << 2 << 3));
			Assert::AreEqual(0, strcmp("qwe123", ($() << "qwe" << 1 << 2 << 3).c_str()));
		}

		TEST_METHOD(exceptTest)
		{
			try
			{
				throw $() << "qwe" << 1 << 2 << 3;
			}
			catch(const std::exception& ex)
			{
				Assert::AreEqual(std::string("qwe123"), std::string(ex.what()));
			}
		}
	};
}