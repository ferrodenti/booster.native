#include "stdafx.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Microsoft { namespace VisualStudio { namespace CppUnitTestFramework 
{
	template<> static std::wstring ToString<StringEx>(const class StringEx& t) { return t.toWString(); }
}}}

namespace unit_tests
{		
	TEST_CLASS(StringExTests)
	{
	public:
		TEST_METHOD(some)
		{
			Assert::IsTrue($("the quick brown fox jumps over the lazy dog").some());
			Assert::IsFalse($("").some());
		}

		TEST_METHOD(comaparison)
		{
			Assert::IsTrue($("the quick brown fox jumps over the lazy dog") == "the quick brown fox jumps over the lazy dog");
			Assert::IsTrue($("the quick brown fox jumps over the lazy dog") == $("the quick brown fox jumps over the lazy dog"));
			Assert::IsTrue($("the quick brown fox jumps over the lazy dog") != $("_the quick brown fox jumps over the lazy dog"));
			Assert::IsTrue($("the quick brown fox jumps over the lazy dog") != "the quick brown fox jumps over the lazy dog_");
		}

		TEST_METHOD(equals)
		{
			Assert::IsTrue($("the quick brown fox jumps over the lazy dog").equals("the quick brown fox jumps over the lazy dog"));
			Assert::IsTrue($("the quick brown fox jumps over the lazy dog").equals($("the quick brown fox jumps over the lazy dog")));
			Assert::IsFalse($("the quick brown fox jumps over_ the lazy dog").equals($("the quick brown fox jumps over the lazy dog")));
		}

		TEST_METHOD(equalsNoCase)
		{
			Assert::IsTrue($("the quick BROWN fox jumps over the lazy dog").equalsNoCase("the quick BROWN fox jumps OVER the lazy DOG"));
			Assert::IsTrue($("THE quick brown fox jumps over the LAZY dog").equalsNoCase($("the QUICK brown fox JUMPS over the lazy dog")));
			Assert::IsFalse($("THE quick brown fox jumps over the LAZY dog").equalsNoCase($("the QUICK brown fox_ JUMPS over the lazy dog")));
		}

		TEST_METHOD(toLower)
		{
			std::string s("THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG");
			Assert::AreEqual($("the quick brown fox jumps over the lazy dog"), $(s).toLower());
		}

		TEST_METHOD(toUpper)
		{
			std::string s("the quick brown fox jumps over the lazy dog");
			const std::string& cs = s; 
			Assert::AreEqual($("THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG"), $(cs).toUpper());
		}

		TEST_METHOD(trims)
		{
			Assert::AreEqual($("the quick brown fox jumps over the lazy dog"), $(" \r\n\t the quick brown fox jumps over the lazy dog").trimStart());
			Assert::AreEqual($("the quick brown fox jumps over the lazy dog"), $("the quick brown fox jumps over the lazy dog \r\n\t ").trimEnd());

			Assert::AreEqual($("quick brown fox jumps over the lazy dog"), $("the quick brown fox jumps over the lazy dog").trimStart("eht "));
			Assert::AreEqual($("the quick brown fox jumps over the lazy"), $("the quick brown fox jumps over the lazy dog").trimEnd("god "));
			Assert::AreEqual($("the quick brown fox jumps over the lazy dog"), $("\r\n\t the quick brown fox jumps over the lazy dog\r\n\t ").trim());
		}
		TEST_METHOD(indexOf)
		{
			std::string s("the quick brown fox jumps over the lazy dog");
			Assert::AreEqual(4, int($(s).indexOf("quick")));
			Assert::AreEqual(4, int($(s).indexOf('q')));
			Assert::AreEqual(31, int($(s).indexOf("the", 1)));
			Assert::AreEqual(31, int($(s).indexOf('t', 1)));

			Assert::AreEqual(31, int($(s).lastIndexOf("the")));
			Assert::AreEqual(31, int($(s).lastIndexOf('t')));
			Assert::AreEqual(0, int($(s).lastIndexOf("the", 30)));
			Assert::AreEqual(0, int($(s).lastIndexOf('t', 30)));
		}

		//TODO: split tests
	};
}