#include "stdafx.h"
#include "..\TestNoCopy.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace unit_tests
{		
	TEST_CLASS(UnorderedMapExTests)
	{
	public:
		std::unordered_map<std::string, std::string> simple_dict = {
			{ "01", "ABC" },
			{ "02", "BCD" },
			{ "03", "CDE" },
			{ "04", "DEF" },
			{ "05", "EFG" },
		};

		TEST_METHOD(keysAndValues)
		{
			Assert::IsTrue($(simple_dict).keys().equals({ "01","02", "03", "04", "05" }));
			Assert::IsTrue($(simple_dict).values().equals({ "ABC", "BCD", "CDE", "DEF", "EFG" }));
		}

		TEST_METHOD(tryGetValue_strings)
		{
			std::string *res = nullptr;
			Assert::IsTrue($(simple_dict).tryGetValue("03", res));
			Assert::IsTrue(*res == "CDE");

			const auto& const_dict = simple_dict;

			const std::string *cres = nullptr;
			Assert::IsTrue($(const_dict).tryGetValue("04", cres));
			Assert::IsTrue(*cres == "DEF");
		}

		TEST_METHOD(getOrAdd_strings)
		{
			std::string *res;
			std::string str6("FGH");
			auto& ps = $(simple_dict).getOrAdd("06", [&str6]() { return &str6; });
			Assert::IsTrue($(simple_dict).tryGetValue("06", res));
			Assert::IsTrue(*res == ps);

			ps = $(simple_dict).getOrAdd("07", []() { return "007"; });
			Assert::IsTrue($(simple_dict).tryGetValue("07", res));
			Assert::IsTrue(*res == "007");
		}

		TEST_METHOD(refs)
		{
			TestNoCopy v1("ABC");
			TestNoCopy v2("BCD");
			TestNoCopy v3("CDE");
			TestNoCopy v4("DEF");
			TestNoCopy v5("EFG");
			TestNoCopy v6("FGH");

			std::unordered_map<std::string, const TestNoCopy&> ref_dict = {
				{ "01", v1 },
				{ "02", v2 },
				{ "03", v3 },
				{ "04", v4 },
				{ "05", v5 },
			};

			TestNoCopy *v = nullptr;
			Assert::IsTrue($(ref_dict).tryGetValue("03", v));
			Assert::IsTrue(v == &v3);

			auto& p = $(ref_dict).getOrAdd("06", [&v6](){ return &v6; });
			Assert::IsTrue($(ref_dict).tryGetValue("06", v));
			Assert::IsTrue(*v == p);

			const auto& c_ref_dict = ref_dict;

			const TestNoCopy *cv;
			Assert::IsTrue($(c_ref_dict).tryGetValue("03", cv));
			Assert::IsTrue(cv == &v3);
		}

		TEST_METHOD(pointers)
		{
			TestNoCopy v1("ABC");
			TestNoCopy v2("BCD");
			TestNoCopy v3("CDE");
			TestNoCopy v4("DEF");
			TestNoCopy v5("EFG");
			TestNoCopy v6("FGH");

			std::unordered_map<std::string, TestNoCopy*> ref_dict = {
				{ "01", &v1 },
				{ "02", &v2 },
				{ "03", &v3 },
				{ "04", &v4 },
				{ "05", &v5 },
			};

			TestNoCopy *v;
			Assert::IsTrue($(ref_dict).tryGetValue("03", v));
			Assert::IsTrue(v == &v3);

			auto& p = $(ref_dict).getOrAdd("06", [&v6]() { return &v6; });
			Assert::IsTrue($(ref_dict).tryGetValue("06", v));
			Assert::IsTrue(*v == p);

			const auto& c_ref_dict = ref_dict;

			const TestNoCopy *cv;
			Assert::IsTrue($(c_ref_dict).tryGetValue("03", cv));
			Assert::IsTrue(cv == &v3);
		}
	};
}