#include "stdafx.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace unit_tests
{		
	TEST_CLASS(VectorExTests)
	{
	public:

		TEST_METHOD(VectorExEqualsTest)
		{
			srand(u32(time(nullptr)));

			for(int i=0; i<1000; ++i)
			{
				int sz = 5 + rand() % 20;
				std::vector<int> a, b;
				for(int j = 0; j<sz; ++j)
					a.push_back(rand());

				bool equals = true;
				if(rand()%3 == 0)
				{
					b.resize(sz);
					copy(a.begin(), a.end(), b.begin());
				}
				else
				{
					int sz2 = 5 + rand() % 20;
					if(sz2 != sz)
						equals = false;

					for (int j = 0; j<sz2; ++j)
					{
						int k = rand();
						b.push_back(k);

						if(!equals || a[j] != b[j])
							equals = false;
					}
				}

				//Assert::AreEqual(equals, $(a).equals(b));
				Assert::AreEqual(equals, $(a).equals($(b)));
			}
		}
	};
}