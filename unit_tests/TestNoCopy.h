#pragma once

namespace unit_tests
{
	class TestNoCopy : public NoCopy
	{
		std::string _str;
	public:

		TestNoCopy(const std::string& str) : _str(str) {}

		bool operator==(const TestNoCopy &other) const
		{
			return _str == other._str;
		}

		std::string toString() const
		{
			return _str;
		}
		std::wstring toWString() const
		{
			return $(_str).toWString();
		}

	};
}

namespace Microsoft { namespace VisualStudio { namespace CppUnitTestFramework 
{
	template<> static std::wstring ToString<unit_tests::TestNoCopy>(const class unit_tests::TestNoCopy& t) { return t.toWString(); }
}}}
